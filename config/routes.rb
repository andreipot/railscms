Rails.application.routes.draw do

  get 'home/index'

  resources :site_configs

  resources :page_meta_infos

  resources :home_sliders

  resources :work_sliders

  resources :team_sliders

  resources :service_sliders

  devise_for :admin_users, ActiveAdmin::Devise.config
  ActiveAdmin.routes(self)


  #resources :services
  get '/service', :to => "services#index", :as => :services
  get	'/service/:short_link(.:format)', :to=>'services#show', :as=>:service


  #resources :teams
  get '/team', :to => "teams#index", :as => :teams
  get	'/team/:short_link(.:format)', :to=>'teams#show', :as=>:team

  #resources :works,:only=>[:index, :show], except: :destroy
  get '/work', :to => "works#index", :as => :works
  get	'/work/:short_link(.:format)', :to=>'works#show', :as=>:work

  #resources :contacts
  get '/contact', :to => "contacts#new", :as => :new_contact
  post '/contact', :to => "contacts#create", :as => :create_contact
  resources :contacts, :only => [:new, :create], :path => 'contacts'

  #resources :project_planners
  get '/project-planner', :to => "project_planners#new", :as => :new_project_planner
  post '/project-planner', :to => "project_planners#create", :as => :create_project_planner
  resources :project_planners, :only => [:new, :create], :path => 'project_planners'

  root 'home#index'
end
