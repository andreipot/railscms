--
-- PostgreSQL database dump
--

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: active_admin_comments; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE active_admin_comments (
    id integer NOT NULL,
    namespace character varying(255),
    body text,
    resource_id character varying(255) NOT NULL,
    resource_type character varying(255) NOT NULL,
    author_id integer,
    author_type character varying(255),
    created_at timestamp without time zone,
    updated_at timestamp without time zone
);


ALTER TABLE public.active_admin_comments OWNER TO postgres;

--
-- Name: active_admin_comments_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE active_admin_comments_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.active_admin_comments_id_seq OWNER TO postgres;

--
-- Name: active_admin_comments_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE active_admin_comments_id_seq OWNED BY active_admin_comments.id;


--
-- Name: admin_users; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE admin_users (
    id integer NOT NULL,
    email character varying(255) DEFAULT ''::character varying NOT NULL,
    encrypted_password character varying(255) DEFAULT ''::character varying NOT NULL,
    reset_password_token character varying(255),
    reset_password_sent_at timestamp without time zone,
    remember_created_at timestamp without time zone,
    sign_in_count integer DEFAULT 0 NOT NULL,
    current_sign_in_at timestamp without time zone,
    last_sign_in_at timestamp without time zone,
    current_sign_in_ip inet,
    last_sign_in_ip inet,
    created_at timestamp without time zone,
    updated_at timestamp without time zone
);


ALTER TABLE public.admin_users OWNER TO postgres;

--
-- Name: admin_users_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE admin_users_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.admin_users_id_seq OWNER TO postgres;

--
-- Name: admin_users_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE admin_users_id_seq OWNED BY admin_users.id;


--
-- Name: contacts; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE contacts (
    id integer NOT NULL,
    email character varying(255),
    full_name character varying(255),
    company character varying(255),
    subject character varying(255),
    message text,
    created_at timestamp without time zone,
    updated_at timestamp without time zone
);


ALTER TABLE public.contacts OWNER TO postgres;

--
-- Name: contacts_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE contacts_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.contacts_id_seq OWNER TO postgres;

--
-- Name: contacts_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE contacts_id_seq OWNED BY contacts.id;


--
-- Name: home_sliders; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE home_sliders (
    id integer NOT NULL,
    layout character varying(255),
    title character varying(255),
    description text,
    link character varying(255),
    tags character varying(255),
    image_file_name character varying(255),
    image_content_type character varying(255),
    image_file_size integer,
    image_updated_at timestamp without time zone,
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    "order" integer
);


ALTER TABLE public.home_sliders OWNER TO postgres;

--
-- Name: home_sliders_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE home_sliders_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.home_sliders_id_seq OWNER TO postgres;

--
-- Name: home_sliders_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE home_sliders_id_seq OWNED BY home_sliders.id;


--
-- Name: page_meta_infos; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE page_meta_infos (
    id integer NOT NULL,
    uri character varying(255),
    keywords character varying(255),
    description character varying(255),
    title character varying(255),
    created_at timestamp without time zone,
    updated_at timestamp without time zone
);


ALTER TABLE public.page_meta_infos OWNER TO postgres;

--
-- Name: page_meta_infos_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE page_meta_infos_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.page_meta_infos_id_seq OWNER TO postgres;

--
-- Name: page_meta_infos_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE page_meta_infos_id_seq OWNED BY page_meta_infos.id;


--
-- Name: page_settings; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE page_settings (
    id integer NOT NULL,
    created_at timestamp without time zone,
    updated_at timestamp without time zone
);


ALTER TABLE public.page_settings OWNER TO postgres;

--
-- Name: page_settings_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE page_settings_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.page_settings_id_seq OWNER TO postgres;

--
-- Name: page_settings_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE page_settings_id_seq OWNED BY page_settings.id;


--
-- Name: project_planners; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE project_planners (
    id integer NOT NULL,
    full_name character varying(255),
    company character varying(255),
    phone_number character varying(255),
    email_address character varying(255),
    hear_about_us character varying(255),
    project_url character varying(255),
    launch_date character varying(255),
    budget integer,
    site_concept text,
    main_objective text,
    schedule_considerations text,
    created_at timestamp without time zone,
    updated_at timestamp without time zone
);


ALTER TABLE public.project_planners OWNER TO postgres;

--
-- Name: project_planners_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE project_planners_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.project_planners_id_seq OWNER TO postgres;

--
-- Name: project_planners_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE project_planners_id_seq OWNED BY project_planners.id;


--
-- Name: schema_migrations; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE schema_migrations (
    version character varying(255) NOT NULL
);


ALTER TABLE public.schema_migrations OWNER TO postgres;

--
-- Name: service_sliders; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE service_sliders (
    id integer NOT NULL,
    service_short_description character varying(255),
    "order" integer,
    active_class character varying(255),
    active_html text,
    navigation_image_file_name character varying(255),
    navigation_image_content_type character varying(255),
    navigation_image_file_size integer,
    navigation_image_updated_at timestamp without time zone,
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    service_id integer
);


ALTER TABLE public.service_sliders OWNER TO postgres;

--
-- Name: service_sliders_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE service_sliders_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.service_sliders_id_seq OWNER TO postgres;

--
-- Name: service_sliders_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE service_sliders_id_seq OWNED BY service_sliders.id;


--
-- Name: services; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE services (
    id integer NOT NULL,
    short_link character varying(255),
    name character varying(255),
    "order" integer,
    homepage_icon_file_name character varying(255),
    homepage_icon_content_type character varying(255),
    homepage_icon_file_size integer,
    homepage_icon_updated_at timestamp without time zone,
    homepage_icon_hover_file_name character varying(255),
    homepage_icon_hover_content_type character varying(255),
    homepage_icon_hover_file_size integer,
    homepage_icon_hover_updated_at timestamp without time zone,
    homepage_description character varying(255),
    detail_top_description character varying(255),
    detail_icon_css_class character varying(255),
    detail_first_heading character varying(255),
    detail_first_paragraph character varying(255),
    detail_second_heading character varying(255),
    detail_second_paragraph character varying(255),
    detail_third_heading character varying(255),
    detail_third_paragrah character varying(255),
    created_at timestamp without time zone,
    updated_at timestamp without time zone
);


ALTER TABLE public.services OWNER TO postgres;

--
-- Name: services_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE services_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.services_id_seq OWNER TO postgres;

--
-- Name: services_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE services_id_seq OWNED BY services.id;


--
-- Name: site_configs; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE site_configs (
    id integer NOT NULL,
    key character varying(255),
    value character varying(255),
    created_at timestamp without time zone,
    updated_at timestamp without time zone
);


ALTER TABLE public.site_configs OWNER TO postgres;

--
-- Name: site_configs_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE site_configs_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.site_configs_id_seq OWNER TO postgres;

--
-- Name: site_configs_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE site_configs_id_seq OWNED BY site_configs.id;


--
-- Name: team_sliders; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE team_sliders (
    id integer NOT NULL,
    team_member_short_link character varying(255),
    "order" integer,
    image_file_name character varying(255),
    image_content_type character varying(255),
    image_file_size integer,
    image_updated_at timestamp without time zone,
    title character varying(255),
    work_short_link character varying(255),
    website_text character varying(255),
    website_link character varying(255),
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    team_id integer
);


ALTER TABLE public.team_sliders OWNER TO postgres;

--
-- Name: team_sliders_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE team_sliders_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.team_sliders_id_seq OWNER TO postgres;

--
-- Name: team_sliders_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE team_sliders_id_seq OWNED BY team_sliders.id;


--
-- Name: teams; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE teams (
    id integer NOT NULL,
    short_link character varying(255),
    "position" character varying(255),
    "order" integer,
    image_file_name character varying(255),
    image_content_type character varying(255),
    image_file_size integer,
    image_updated_at timestamp without time zone,
    team_page_bio character varying(255),
    detail_page_website_text character varying(255),
    detail_page_website_url character varying(255),
    detail_page_bio text,
    facebook_url character varying(255),
    twitter_url character varying(255),
    gplus_url character varying(255),
    dribble_url character varying(255),
    github_url character varying(255),
    created_at timestamp without time zone,
    updated_at timestamp without time zone
);


ALTER TABLE public.teams OWNER TO postgres;

--
-- Name: teams_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE teams_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.teams_id_seq OWNER TO postgres;

--
-- Name: teams_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE teams_id_seq OWNED BY teams.id;


--
-- Name: users; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE users (
    id integer NOT NULL,
    email character varying(255) DEFAULT ''::character varying NOT NULL,
    encrypted_password character varying(255) DEFAULT ''::character varying NOT NULL,
    reset_password_token character varying(255),
    reset_password_sent_at timestamp without time zone,
    remember_created_at timestamp without time zone,
    sign_in_count integer DEFAULT 0 NOT NULL,
    current_sign_in_at timestamp without time zone,
    last_sign_in_at timestamp without time zone,
    current_sign_in_ip inet,
    last_sign_in_ip inet,
    created_at timestamp without time zone,
    updated_at timestamp without time zone
);


ALTER TABLE public.users OWNER TO postgres;

--
-- Name: users_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE users_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.users_id_seq OWNER TO postgres;

--
-- Name: users_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE users_id_seq OWNED BY users.id;


--
-- Name: work_sliders; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE work_sliders (
    id integer NOT NULL,
    work_short_link character varying(255),
    "order" integer,
    navigation_image_file_name character varying(255),
    navigation_image_content_type character varying(255),
    navigation_image_file_size integer,
    navigation_image_updated_at timestamp without time zone,
    navigation_url character varying(255),
    active_class character varying(255),
    active_html text,
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    work_id integer
);


ALTER TABLE public.work_sliders OWNER TO postgres;

--
-- Name: work_sliders_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE work_sliders_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.work_sliders_id_seq OWNER TO postgres;

--
-- Name: work_sliders_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE work_sliders_id_seq OWNED BY work_sliders.id;


--
-- Name: works; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE works (
    id integer NOT NULL,
    short_link character varying(255),
    title character varying(255),
    tags character varying(255),
    "order" integer,
    list_page_description text,
    detail_page_link_text character varying(255),
    detail_page_link_url character varying(255),
    detail_page_description text,
    image_file_name character varying(255),
    image_content_type character varying(255),
    image_file_size integer,
    image_updated_at timestamp without time zone,
    created_at timestamp without time zone,
    updated_at timestamp without time zone
);


ALTER TABLE public.works OWNER TO postgres;

--
-- Name: works_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE works_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.works_id_seq OWNER TO postgres;

--
-- Name: works_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE works_id_seq OWNED BY works.id;


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY active_admin_comments ALTER COLUMN id SET DEFAULT nextval('active_admin_comments_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY admin_users ALTER COLUMN id SET DEFAULT nextval('admin_users_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY contacts ALTER COLUMN id SET DEFAULT nextval('contacts_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY home_sliders ALTER COLUMN id SET DEFAULT nextval('home_sliders_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY page_meta_infos ALTER COLUMN id SET DEFAULT nextval('page_meta_infos_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY page_settings ALTER COLUMN id SET DEFAULT nextval('page_settings_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY project_planners ALTER COLUMN id SET DEFAULT nextval('project_planners_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY service_sliders ALTER COLUMN id SET DEFAULT nextval('service_sliders_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY services ALTER COLUMN id SET DEFAULT nextval('services_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY site_configs ALTER COLUMN id SET DEFAULT nextval('site_configs_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY team_sliders ALTER COLUMN id SET DEFAULT nextval('team_sliders_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY teams ALTER COLUMN id SET DEFAULT nextval('teams_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY users ALTER COLUMN id SET DEFAULT nextval('users_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY work_sliders ALTER COLUMN id SET DEFAULT nextval('work_sliders_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY works ALTER COLUMN id SET DEFAULT nextval('works_id_seq'::regclass);


--
-- Data for Name: active_admin_comments; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY active_admin_comments (id, namespace, body, resource_id, resource_type, author_id, author_type, created_at, updated_at) FROM stdin;
1	admin	comment on my user	4	AdminUser	2	AdminUser	2015-03-17 22:11:10.338613	2015-03-17 22:11:10.338613
\.


--
-- Name: active_admin_comments_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('active_admin_comments_id_seq', 1, true);


--
-- Data for Name: admin_users; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY admin_users (id, email, encrypted_password, reset_password_token, reset_password_sent_at, remember_created_at, sign_in_count, current_sign_in_at, last_sign_in_at, current_sign_in_ip, last_sign_in_ip, created_at, updated_at) FROM stdin;
2	ven.inova303@gmail.com	$2a$10$i87WCfZ2mPqutOLtdTDI.eYVuyTyD21nh.DyHQTuKVXzHqGWa4BL2	\N	\N	2015-03-17 22:09:59.935476	2	2015-03-17 22:09:59.937905	2015-03-17 22:08:08.246908	173.198.16.11	184.180.218.52	2015-03-17 15:19:00.22182	2015-03-17 22:09:59.938466
4	jbrahy@thinkasdf.com	$2a$10$pH.E/DIHmfoXg4qcM1AbVeObHJ5uSpCqKZ4uMP1VLi7WyH8oYzN.K	\N	\N	\N	0	\N	\N	\N	\N	2015-03-17 22:10:27.21797	2015-03-17 22:10:27.21797
3	anthony@thinkasdf.com	$2a$10$TlyCCC2Jq8VNcduZeWGVje010QOsJvMLZIKjyuFDEI6CAaTrNJ/cC	b1067978bb5e1f1928fa3c43d72e4f0d0f5b1098bb4575ad629fb2e72079a03b	2015-03-31 19:37:00.328383	\N	13	2015-04-01 19:09:03.809309	2015-04-01 19:07:32.647218	184.180.218.52	184.180.218.52	2015-03-17 22:09:00.838375	2015-04-01 19:09:03.809977
1	admin@example.com	$2a$10$DE/YrKAtf6Dz6Im6yH4/W.MBe1A3gYI4ujUOnw0LJhYRV7idTgssm	\N	\N	\N	6	2015-04-03 19:57:31.113139	2015-04-03 14:19:16.544828	24.244.32.63	193.108.24.242	2015-03-17 14:02:48.79119	2015-04-03 19:57:31.113836
\.


--
-- Name: admin_users_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('admin_users_id_seq', 5, true);


--
-- Data for Name: contacts; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY contacts (id, email, full_name, company, subject, message, created_at, updated_at) FROM stdin;
1	test@gmail.com	Tester	Test Comapny	Full Stack Developer	Hello we need full stack developer.	2015-03-20 20:40:51.396084	2015-03-20 20:40:51.396084
2	anthony@thinkasdf.com	Anthony Iacono		Test subject	Test	2015-03-20 20:41:41.694781	2015-03-24 21:57:05.345151
3	ven.inova303@gmail.com	Ven Ivanov	test company	test app	app test is for ur	2015-03-27 14:34:04.403337	2015-03-27 14:34:04.403337
4	asdf	asdf	asdf	3333	asdfadsf	2015-03-27 14:59:39.225797	2015-03-27 14:59:39.225797
5	sadf	asdf	asdfs	asdf	sadf	2015-03-27 15:05:26.370292	2015-03-27 15:05:26.370292
6	asdfasdf	asdf	asdfasdf	asdf	asdf	2015-03-27 15:48:49.743918	2015-03-27 15:48:49.743918
9	asdf@gmail.com	sadf  asdf adsf	asdfa	asdfasdf	asdfasf	2015-03-27 23:33:34.410208	2015-03-27 23:33:34.410208
20	sdafasdf@test.com	3333	asasdfasdf	adsfasdf	asdfasdf	2015-03-30 16:43:56.856474	2015-03-30 16:43:56.856474
\.


--
-- Name: contacts_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('contacts_id_seq', 20, true);


--
-- Data for Name: home_sliders; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY home_sliders (id, layout, title, description, link, tags, image_file_name, image_content_type, image_file_size, image_updated_at, created_at, updated_at, "order") FROM stdin;
5	slide-intro-layout	We Are ASDF	Like what you see yet?			\N	\N	\N	\N	2015-03-31 18:17:50.736613	2015-03-31 18:17:50.736613	1
3	slide-column-layout	Nike+ Runner Services	Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla tempor justo pharetra rutrum tempor. In a condimentum erat. Nam non urna nulla.	/work/nike-runner-services	creative-design, app-development	homepage-slider-sample.jpg	image/jpeg	179745	2015-03-30 16:06:12.493251	2015-03-30 16:05:38.839869	2015-03-31 18:19:09.061627	2
\.


--
-- Name: home_sliders_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('home_sliders_id_seq', 5, true);


--
-- Data for Name: page_meta_infos; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY page_meta_infos (id, uri, keywords, description, title, created_at, updated_at) FROM stdin;
\.


--
-- Name: page_meta_infos_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('page_meta_infos_id_seq', 1, false);


--
-- Data for Name: page_settings; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY page_settings (id, created_at, updated_at) FROM stdin;
\.


--
-- Name: page_settings_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('page_settings_id_seq', 1, false);


--
-- Data for Name: project_planners; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY project_planners (id, full_name, company, phone_number, email_address, hear_about_us, project_url, launch_date, budget, site_concept, main_objective, schedule_considerations, created_at, updated_at) FROM stdin;
1	Anthony Iacono	Association of Software Development Fanatics	(310) 707-3289	anthony@thinkasdf.com	Google	http://thinkasdf.com	04-01-2015	5000	TODO: This should support multiple lines.	TODO: This should support multiple lines.	TODO: This should support multiple lines.	2015-03-20 21:13:31.564927	2015-03-24 22:00:58.128854
2	asdf asdf asdf		123412423	test@gmail.com				50000				2015-03-27 23:39:52.668515	2015-03-27 23:39:52.668515
6	333 333 3333 333	ASDFsadf	23424234234	admin@test.com	asdfasdf			\N				2015-03-30 18:33:26.45011	2015-03-30 18:33:26.45011
\.


--
-- Name: project_planners_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('project_planners_id_seq', 6, true);


--
-- Data for Name: schema_migrations; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY schema_migrations (version) FROM stdin;
20150316081124
20150316082710
20150316085125
20150316085651
20150316090150
20150316090403
20150316151621
20150316152325
20150317140238
20150317140241
20150318194620
20150318194804
20150318195149
20150318200331
20150321000315
20150324231112
20150325001307
20150321001218
20150325004509
20150325013206
20150325015341
20150326135951
20150326140739
20150327212706
20150327212741
20150327212755
20150401201735
\.


--
-- Data for Name: service_sliders; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY service_sliders (id, service_short_description, "order", active_class, active_html, navigation_image_file_name, navigation_image_content_type, navigation_image_file_size, navigation_image_updated_at, created_at, updated_at, service_id) FROM stdin;
2		1	sample-class	<img src="/img/service-detail/slider-sample.jpg">	sample-navi.jpg	image/jpeg	132025	2015-04-01 19:34:06.914272	2015-04-01 19:33:05.257812	2015-04-01 19:34:07.082394	1
3		1	sample-class	<img src="/img/service-detail/slider-sample.jpg">	sample-navi.jpg	image/jpeg	132025	2015-04-01 19:35:20.17978	2015-04-01 19:35:20.375652	2015-04-01 19:35:47.897799	2
4		1	sample-class	<img src="/img/service-detail/slider-sample.jpg">	sample-navi.jpg	image/jpeg	132025	2015-04-01 19:36:15.792229	2015-04-01 19:36:15.92661	2015-04-01 19:36:15.92661	3
5		1	sample-class	<img src="/img/service-detail/slider-sample.jpg">	sample-navi.jpg	image/jpeg	132025	2015-04-01 19:36:50.149629	2015-04-01 19:36:50.283405	2015-04-01 19:36:50.283405	4
6		1	sample-class	<img src="/img/service-detail/slider-sample.jpg">	sample-navi.jpg	image/jpeg	132025	2015-04-01 19:37:56.75971	2015-04-01 19:37:56.891302	2015-04-01 19:37:56.891302	5
7		1	sample-class	<img src="/img/service-detail/slider-sample.jpg">	sample-navi.jpg	image/jpeg	132025	2015-04-01 19:38:17.083696	2015-04-01 19:38:17.215104	2015-04-01 19:38:17.215104	6
8		1	sample-class	<img src="/img/service-detail/slider-sample.jpg">	sample-navi.jpg	image/jpeg	132025	2015-04-01 19:38:34.170563	2015-04-01 19:38:34.30135	2015-04-01 19:38:34.30135	7
9		1	sample-class	<img src="/img/service-detail/slider-sample.jpg">	sample-navi.jpg	image/jpeg	132025	2015-04-01 19:38:50.359447	2015-04-01 19:38:50.491255	2015-04-01 19:38:50.491255	8
\.


--
-- Name: service_sliders_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('service_sliders_id_seq', 9, true);


--
-- Data for Name: services; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY services (id, short_link, name, "order", homepage_icon_file_name, homepage_icon_content_type, homepage_icon_file_size, homepage_icon_updated_at, homepage_icon_hover_file_name, homepage_icon_hover_content_type, homepage_icon_hover_file_size, homepage_icon_hover_updated_at, homepage_description, detail_top_description, detail_icon_css_class, detail_first_heading, detail_first_paragraph, detail_second_heading, detail_second_paragraph, detail_third_heading, detail_third_paragrah, created_at, updated_at) FROM stdin;
1	web-development	Web Development	1	webdev.png	image/png	6142	2015-03-24 22:06:24.751046	webdev_hover.png	image/png	5956	2015-03-24 22:06:25.226292	Lorem ipsum dolor sit amet, con sectetur ad elit ipiscing.	Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus nisl velit, suscipit a orci et, rhoncus dictum ante. Aliquam porta ex enim, quis facilisis neque eleifend volutpat.	web-dev	Pellentes Que	Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus nisl velit, suscipit a orci et, rhoncus dictum ante. Aliquam porta ex enim, quis facilisis neque eleifend volutpat. Integer consectetur metus diam, id mollis lectus dignissim at.	Class Aptent	Duis eu dolor eu ligula feugiat consequat. Suspendisse a leo massa. Donec vitae tempor est, a dapibus enim. Mauris quis velit imperdiet, finibus dolor eu, hendrerit nisi. Vivamus eget imperdiet dolor, ut accumsan tellus. Cras eleifend leo justo.	Suspendisse A Leo	Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus nisl velit, suscipit a orci et, rhoncus dictum ante. Aliquam porta ex enim, quis facilisis neque eleifend volutpat. Integer consectetur metus diam, id mollis lectus dignissim at. In in era	2015-03-21 09:27:12.131741	2015-03-26 19:53:00.95816
2	app-development	App Development	2	webhosting.png	image/png	6156	2015-03-26 22:07:44.449773	webhosting_hover.png	image/png	5997	2015-03-26 22:08:08.686546	Lorem ipsum dolor sit amet, con sectetur ad elit ipiscing.	Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus nisl velit, suscipit a orci et, rhoncus dictum ante. Aliquam porta ex enim, quis facilisis neque eleifend volutpat.	web-dev	Pellentes Que	Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus nisl velit, suscipit a orci et, rhoncus dictum ante. Aliquam porta ex enim, quis facilisis neque eleifend volutpat. Integer consectetur metus diam, id mollis lectus dignissim at.	Class Aptent	Duis eu dolor eu ligula feugiat consequat. Suspendisse a leo massa. Donec vitae tempor est, a dapibus enim. Mauris quis velit imperdiet, finibus dolor eu, hendrerit nisi. Vivamus eget imperdiet dolor, ut accumsan tellus. Cras eleifend leo justo.	Suspendisse A Leo	Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus nisl velit, suscipit a orci et, rhoncus dictum ante. Aliquam porta ex enim, quis facilisis neque eleifend volutpat. Integer consectetur metus diam, id mollis lectus dignissim at. In in era	2015-03-21 09:27:12.131741	2015-03-27 23:02:03.057213
3	creative-design	Creative Design	3	creativedesign.png	image/png	5850	2015-04-01 19:15:19.262323	creativedesign_hover.png	image/png	5753	2015-04-01 19:15:20.004686	Lorem ipsum dolor sit amet, con sectetur ad elit ipiscing.	Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus nisl velit, suscipit a orci et, rhoncus dictum ante. Aliquam porta ex enim, quis facilisis neque eleifend volutpat.	creative-design	PELLENTES QUE	Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus nisl velit, suscipit a orci et, rhoncus dictum ante. Aliquam porta ex enim, quis facilisis neque eleifend volutpat. Integer consectetur metus diam, id mollis lectus dignissim at.	CLASS APTENT	Duis eu dolor eu ligula feugiat consequat. Suspendisse a leo massa. Donec vitae tempor est, a dapibus enim. Mauris quis velit imperdiet, finibus dolor eu, hendrerit nisi. Vivamus eget imperdiet dolor, ut accumsan tellus. Cras eleifend leo justo.	SUSPENDISSE A LEO	Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus nisl velit, suscipit a orci et, rhoncus dictum ante. Aliquam porta ex enim, quis facilisis neque eleifend volutpat. Integer consectetur metus diam, id mollis lectus dignissim at. In in era	2015-04-01 19:15:20.1183	2015-04-01 19:15:20.1183
4	web-hosting	Web Hosting	4	webhosting.png	image/png	6156	2015-04-01 19:20:24.083631	webhosting_hover.png	image/png	5997	2015-04-01 19:20:24.210363	Lorem ipsum dolor sit amet, con sectetur ad elit ipiscing.	Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus nisl velit, suscipit a orci et, rhoncus dictum ante. Aliquam porta ex enim, quis facilisis neque eleifend volutpat.	web-hosting	PELLENTES QUE	Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus nisl velit, suscipit a orci et, rhoncus dictum ante. Aliquam porta ex enim, quis facilisis neque eleifend volutpat. Integer consectetur metus diam, id mollis lectus dignissim at.	CLASS APTENT	Duis eu dolor eu ligula feugiat consequat. Suspendisse a leo massa. Donec vitae tempor est, a dapibus enim. Mauris quis velit imperdiet, finibus dolor eu, hendrerit nisi. Vivamus eget imperdiet dolor, ut accumsan tellus. Cras eleifend leo justo.	SUSPENDISSE A LEO	Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus nisl velit, suscipit a orci et, rhoncus dictum ante. Aliquam porta ex enim, quis facilisis neque eleifend volutpat. Integer consectetur metus diam, id mollis lectus dignissim at. In in era	2015-04-01 19:20:24.323419	2015-04-01 19:20:24.323419
5	project-management	Project Management	5	projmanage.png	image/png	5944	2015-04-01 19:23:55.811404	projmanage_hover.png	image/png	5843	2015-04-01 19:23:55.925466	Lorem ipsum dolor sit amet, con sectetur ad elit ipiscing.	Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus nisl velit, suscipit a orci et, rhoncus dictum ante. Aliquam porta ex enim, quis facilisis neque eleifend volutpat.	proj-manage	PELLENTES QUE	Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus nisl velit, suscipit a orci et, rhoncus dictum ante. Aliquam porta ex enim, quis facilisis neque eleifend volutpat. Integer consectetur metus diam, id mollis lectus dignissim at.	CLASS APTENT	Duis eu dolor eu ligula feugiat consequat. Suspendisse a leo massa. Donec vitae tempor est, a dapibus enim. Mauris quis velit imperdiet, finibus dolor eu, hendrerit nisi. Vivamus eget imperdiet dolor, ut accumsan tellus. Cras eleifend leo justo.	SUSPENDISSE A LEO	Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus nisl velit, suscipit a orci et, rhoncus dictum ante. Aliquam porta ex enim, quis facilisis neque eleifend volutpat. Integer consectetur metus diam, id mollis lectus dignissim at. In in era	2015-04-01 19:23:56.041605	2015-04-01 19:23:56.041605
6	security	Security	6	security.png	image/png	6020	2015-04-01 19:26:04.484273	security_hover.png	image/png	6005	2015-04-01 19:26:04.596863	Lorem ipsum dolor sit amet, con sectetur ad elit ipiscing.	Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus nisl velit, suscipit a orci et, rhoncus dictum ante. Aliquam porta ex enim, quis facilisis neque eleifend volutpat.	security	PELLENTES QUE	Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus nisl velit, suscipit a orci et, rhoncus dictum ante. Aliquam porta ex enim, quis facilisis neque eleifend volutpat. Integer consectetur metus diam, id mollis lectus dignissim at.	CLASS APTENT	Duis eu dolor eu ligula feugiat consequat. Suspendisse a leo massa. Donec vitae tempor est, a dapibus enim. Mauris quis velit imperdiet, finibus dolor eu, hendrerit nisi. Vivamus eget imperdiet dolor, ut accumsan tellus. Cras eleifend leo justo.	SUSPENDISSE A LEO	Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus nisl velit, suscipit a orci et, rhoncus dictum ante. Aliquam porta ex enim, quis facilisis neque eleifend volutpat. Integer consectetur metus diam, id mollis lectus dignissim at. In in era	2015-04-01 19:26:04.710755	2015-04-01 19:26:04.710755
7	data-dev	Database Development	7	datadev.png	image/png	7664	2015-04-01 19:28:40.150003	datadev_hover.png	image/png	7957	2015-04-01 19:28:40.265332	Lorem ipsum dolor sit amet, con sectetur ad elit ipiscing.	Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus nisl velit, suscipit a orci et, rhoncus dictum ante. Aliquam porta ex enim, quis facilisis neque eleifend volutpat.	data-dev	PELLENTES QUE	Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus nisl velit, suscipit a orci et, rhoncus dictum ante. Aliquam porta ex enim, quis facilisis neque eleifend volutpat. Integer consectetur metus diam, id mollis lectus dignissim at.	CLASS APTENT	Duis eu dolor eu ligula feugiat consequat. Suspendisse a leo massa. Donec vitae tempor est, a dapibus enim. Mauris quis velit imperdiet, finibus dolor eu, hendrerit nisi. Vivamus eget imperdiet dolor, ut accumsan tellus. Cras eleifend leo justo.	SUSPENDISSE A LEO	Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus nisl velit, suscipit a orci et, rhoncus dictum ante. Aliquam porta ex enim, quis facilisis neque eleifend volutpat. Integer consectetur metus diam, id mollis lectus dignissim at. In in era	2015-04-01 19:28:40.379403	2015-04-01 19:28:40.379403
8	scalable-clusters	Scalable Clusters	8	scale.png	image/png	5491	2015-04-01 19:30:33.059012	scale_hover.png	image/png	5540	2015-04-01 19:30:33.172505	Lorem ipsum dolor sit amet, con sectetur ad elit ipiscing.	Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus nisl velit, suscipit a orci et, rhoncus dictum ante. Aliquam porta ex enim, quis facilisis neque eleifend volutpat.	scale	PELLENTES QUE	Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus nisl velit, suscipit a orci et, rhoncus dictum ante. Aliquam porta ex enim, quis facilisis neque eleifend volutpat. Integer consectetur metus diam, id mollis lectus dignissim at.	CLASS APTENT	Duis eu dolor eu ligula feugiat consequat. Suspendisse a leo massa. Donec vitae tempor est, a dapibus enim. Mauris quis velit imperdiet, finibus dolor eu, hendrerit nisi. Vivamus eget imperdiet dolor, ut accumsan tellus. Cras eleifend leo justo.	SUSPENDISSE A LEO	Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus nisl velit, suscipit a orci et, rhoncus dictum ante. Aliquam porta ex enim, quis facilisis neque eleifend volutpat. Integer consectetur metus diam, id mollis lectus dignissim at. In in era	2015-04-01 19:30:33.291427	2015-04-01 19:30:33.291427
\.


--
-- Name: services_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('services_id_seq', 8, true);


--
-- Data for Name: site_configs; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY site_configs (id, key, value, created_at, updated_at) FROM stdin;
1	site_name	ASDF	2015-03-25 01:42:52.868346	2015-03-25 01:51:43.815845
2	facebook_url	http://www.facebook.com/#	2015-03-27 20:20:45.828625	2015-03-27 20:20:45.828625
3	github_url	https://github.com/#	2015-03-27 20:23:59.600488	2015-03-27 20:23:59.600488
4	gplus_url	http://www.google.com	2015-03-27 20:24:22.763215	2015-03-27 20:24:22.763215
5	twitter_url	http://www.twitter.com/#	2015-03-27 20:24:46.231535	2015-03-27 20:24:46.231535
6	dribble_url	http:/www.dribble.com/#	2015-03-27 20:25:01.694587	2015-03-27 20:25:01.694587
\.


--
-- Name: site_configs_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('site_configs_id_seq', 7, true);


--
-- Data for Name: team_sliders; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY team_sliders (id, team_member_short_link, "order", image_file_name, image_content_type, image_file_size, image_updated_at, title, work_short_link, website_text, website_link, created_at, updated_at, team_id) FROM stdin;
2	justin	1	homepage-slider-sample.jpg	image/jpeg	179745	2015-04-01 19:45:38.310614	Nike+ Runner Services	runner-services	www.nike.com/nwhm	http://nike.com/nwhm	2015-04-01 19:45:38.468414	2015-04-01 19:48:55.981909	2
3	ven	1	homepage-slider-sample.jpg	image/jpeg	179745	2015-04-01 19:46:25.458882	Nike+ Runner Services	runner-services	www.nike.com/nwhm	http://nike.com/nwhm	2015-04-01 19:46:25.613173	2015-04-01 19:49:07.190232	3
4	orfeas	1	homepage-slider-sample.jpg	image/jpeg	179745	2015-04-01 19:48:17.375275	Nike+ Runner Services	runner-services	www.nike.com/nwhm	http://nike.com/nwhm	2015-04-01 19:46:49.683056	2015-04-01 19:49:17.577008	4
1	anthony	1	navi-sample1.jpg	image/jpeg	86108	2015-03-24 22:10:57.045227	Nike+ Runner Services	runner-services	www.nike.com/nwhm	http://nike.com/nwhm	2015-03-21 02:03:48.882927	2015-04-01 19:49:28.332224	1
\.


--
-- Name: team_sliders_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('team_sliders_id_seq', 4, true);


--
-- Data for Name: teams; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY teams (id, short_link, "position", "order", image_file_name, image_content_type, image_file_size, image_updated_at, team_page_bio, detail_page_website_text, detail_page_website_url, detail_page_bio, facebook_url, twitter_url, gplus_url, dribble_url, github_url, created_at, updated_at) FROM stdin;
1	anthony	Partner	1	anthony.png	image/png	42427	2015-03-24 21:32:32.643507	Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis scelerisque.	www.thinkasdf.com	http://thinkasdf.com	TODO: This needs to be made into a textarea so it can contain multiple paragraphs.	http://facebook.com/#	http://twitter.com/#	http://google.com/+/#	http://dribbble.com/+/#	http://github.com/AnthonyIacono	2015-03-21 12:29:46.066237	2015-03-26 19:53:18.1358
2	Justin	Partner	1	mini-bio-jb.png	image/png	37561	2015-03-26 22:00:20.359445	Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis scelerisque.	www.thinkasdf.com	http://thinkasdf.com	TODO: This needs to be made into a textarea so it can contain multiple paragraphs.	http://facebook.com/#	http://twitter.com/#	http://google.com/+/#	http://dribbble.com/+/#	http://github.com/AnthonyIacono	2015-03-21 12:29:46.066237	2015-03-26 22:00:20.563202
3	Ven	Develoer	1	mini-bio-lucas.png	image/png	32843	2015-03-26 22:01:04.383133	Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis scelerisque.	www.thinkasdf.com	http://thinkasdf.com	TODO: This needs to be made into a textarea so it can contain multiple paragraphs.	http://facebook.com/#	http://twitter.com/#	http://google.com/+/#	http://dribbble.com/+/#	http://github.com/AnthonyIacono	2015-03-21 12:29:46.066237	2015-03-26 22:01:04.51491
4	Orfeas	Partner	1	mini-bio-orfeas.png	image/png	55805	2015-03-26 22:01:31.351033	Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis scelerisque.	www.thinkasdf.com	http://thinkasdf.com	TODO: This needs to be made into a textarea so it can contain multiple paragraphs.	http://facebook.com/#	http://twitter.com/#	http://google.com/+/#	http://dribbble.com/+/#	http://github.com/AnthonyIacono	2015-03-21 12:29:46.066237	2015-03-26 22:01:31.490725
\.


--
-- Name: teams_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('teams_id_seq', 1, true);


--
-- Data for Name: users; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY users (id, email, encrypted_password, reset_password_token, reset_password_sent_at, remember_created_at, sign_in_count, current_sign_in_at, last_sign_in_at, current_sign_in_ip, last_sign_in_ip, created_at, updated_at) FROM stdin;
\.


--
-- Name: users_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('users_id_seq', 1, false);


--
-- Data for Name: work_sliders; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY work_sliders (id, work_short_link, "order", navigation_image_file_name, navigation_image_content_type, navigation_image_file_size, navigation_image_updated_at, navigation_url, active_class, active_html, created_at, updated_at, work_id) FROM stdin;
3		1	slider-navi-sample2.jpg	image/jpeg	70742	2015-04-01 19:55:37.828135	#	sample-class	<img src="/img/work/detail/sample.jpg">	2015-04-01 19:55:37.959801	2015-04-01 22:07:04.041058	2
\.


--
-- Name: work_sliders_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('work_sliders_id_seq', 3, true);


--
-- Data for Name: works; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY works (id, short_link, title, tags, "order", list_page_description, detail_page_link_text, detail_page_link_url, detail_page_description, image_file_name, image_content_type, image_file_size, image_updated_at, created_at, updated_at) FROM stdin;
2	runner-services	Nike+ Runner Services	app-development,creative-design,web-development	1	Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec vitae urna a sem posuere tempus eget vel eros. Nullam rhoncus aliquet tortor, et tempus leo rutrum et. Proin sed dignissim sem. Fusce quis velit augue. Donec nisl nunc, lobortis ac ligula non,	nike.com/nwhm	http://nike.com/nwhm	Sed diam felis, viverra in porta vel, maximus vel quam. Etiam a lacus justo. Maecenas vel purus sit amet dolor luctus congue et nec justo. Integer sem massa, accumsan vitae ex nec, mattis aliquet augue. Aliquam erat volutpat. Cras maximus ante mauris, sed	sample-image.png	image/png	105485	2015-03-24 22:19:44.212886	2015-03-22 12:05:44.579981	2015-03-27 23:02:55.289493
\.


--
-- Name: works_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('works_id_seq', 3, true);


--
-- Name: active_admin_comments_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY active_admin_comments
    ADD CONSTRAINT active_admin_comments_pkey PRIMARY KEY (id);


--
-- Name: admin_users_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY admin_users
    ADD CONSTRAINT admin_users_pkey PRIMARY KEY (id);


--
-- Name: contacts_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY contacts
    ADD CONSTRAINT contacts_pkey PRIMARY KEY (id);


--
-- Name: home_sliders_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY home_sliders
    ADD CONSTRAINT home_sliders_pkey PRIMARY KEY (id);


--
-- Name: page_meta_infos_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY page_meta_infos
    ADD CONSTRAINT page_meta_infos_pkey PRIMARY KEY (id);


--
-- Name: page_settings_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY page_settings
    ADD CONSTRAINT page_settings_pkey PRIMARY KEY (id);


--
-- Name: project_planners_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY project_planners
    ADD CONSTRAINT project_planners_pkey PRIMARY KEY (id);


--
-- Name: service_sliders_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY service_sliders
    ADD CONSTRAINT service_sliders_pkey PRIMARY KEY (id);


--
-- Name: services_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY services
    ADD CONSTRAINT services_pkey PRIMARY KEY (id);


--
-- Name: site_configs_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY site_configs
    ADD CONSTRAINT site_configs_pkey PRIMARY KEY (id);


--
-- Name: team_sliders_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY team_sliders
    ADD CONSTRAINT team_sliders_pkey PRIMARY KEY (id);


--
-- Name: teams_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY teams
    ADD CONSTRAINT teams_pkey PRIMARY KEY (id);


--
-- Name: users_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY users
    ADD CONSTRAINT users_pkey PRIMARY KEY (id);


--
-- Name: work_sliders_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY work_sliders
    ADD CONSTRAINT work_sliders_pkey PRIMARY KEY (id);


--
-- Name: works_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY works
    ADD CONSTRAINT works_pkey PRIMARY KEY (id);


--
-- Name: index_active_admin_comments_on_author_type_and_author_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX index_active_admin_comments_on_author_type_and_author_id ON active_admin_comments USING btree (author_type, author_id);


--
-- Name: index_active_admin_comments_on_namespace; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX index_active_admin_comments_on_namespace ON active_admin_comments USING btree (namespace);


--
-- Name: index_active_admin_comments_on_resource_type_and_resource_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX index_active_admin_comments_on_resource_type_and_resource_id ON active_admin_comments USING btree (resource_type, resource_id);


--
-- Name: index_admin_users_on_email; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE UNIQUE INDEX index_admin_users_on_email ON admin_users USING btree (email);


--
-- Name: index_admin_users_on_reset_password_token; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE UNIQUE INDEX index_admin_users_on_reset_password_token ON admin_users USING btree (reset_password_token);


--
-- Name: index_service_sliders_on_service_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX index_service_sliders_on_service_id ON service_sliders USING btree (service_id);


--
-- Name: index_team_sliders_on_team_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX index_team_sliders_on_team_id ON team_sliders USING btree (team_id);


--
-- Name: index_users_on_email; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE UNIQUE INDEX index_users_on_email ON users USING btree (email);


--
-- Name: index_users_on_reset_password_token; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE UNIQUE INDEX index_users_on_reset_password_token ON users USING btree (reset_password_token);


--
-- Name: index_work_sliders_on_work_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX index_work_sliders_on_work_id ON work_sliders USING btree (work_id);


--
-- Name: unique_schema_migrations; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE UNIQUE INDEX unique_schema_migrations ON schema_migrations USING btree (version);


--
-- Name: public; Type: ACL; Schema: -; Owner: andreikraykov
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM andreikraykov;
GRANT ALL ON SCHEMA public TO andreikraykov;
GRANT ALL ON SCHEMA public TO PUBLIC;


--
-- PostgreSQL database dump complete
--

