require 'test_helper'

class PageMetaInfosControllerTest < ActionController::TestCase
  setup do
    @page_meta_info = page_meta_infos(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:page_meta_infos)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create page_meta_info" do
    assert_difference('PageMetaInfo.count') do
      post :create, page_meta_info: { description: @page_meta_info.description, keywords: @page_meta_info.keywords, title: @page_meta_info.title, uri: @page_meta_info.uri }
    end

    assert_redirected_to page_meta_info_path(assigns(:page_meta_info))
  end

  test "should show page_meta_info" do
    get :show, id: @page_meta_info
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @page_meta_info
    assert_response :success
  end

  test "should update page_meta_info" do
    patch :update, id: @page_meta_info, page_meta_info: { description: @page_meta_info.description, keywords: @page_meta_info.keywords, title: @page_meta_info.title, uri: @page_meta_info.uri }
    assert_redirected_to page_meta_info_path(assigns(:page_meta_info))
  end

  test "should destroy page_meta_info" do
    assert_difference('PageMetaInfo.count', -1) do
      delete :destroy, id: @page_meta_info
    end

    assert_redirected_to page_meta_infos_path
  end
end
