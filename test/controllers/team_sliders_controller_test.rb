require 'test_helper'

class TeamSlidersControllerTest < ActionController::TestCase
  setup do
    @team_slider = team_sliders(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:team_sliders)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create team_slider" do
    assert_difference('TeamSlider.count') do
      post :create, team_slider: { image: @team_slider.image, order: @team_slider.order, team_member_short_link: @team_slider.team_member_short_link, title: @team_slider.title, website_link: @team_slider.website_link, website_text: @team_slider.website_text, work_short_link: @team_slider.work_short_link }
    end

    assert_redirected_to team_slider_path(assigns(:team_slider))
  end

  test "should show team_slider" do
    get :show, id: @team_slider
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @team_slider
    assert_response :success
  end

  test "should update team_slider" do
    patch :update, id: @team_slider, team_slider: { image: @team_slider.image, order: @team_slider.order, team_member_short_link: @team_slider.team_member_short_link, title: @team_slider.title, website_link: @team_slider.website_link, website_text: @team_slider.website_text, work_short_link: @team_slider.work_short_link }
    assert_redirected_to team_slider_path(assigns(:team_slider))
  end

  test "should destroy team_slider" do
    assert_difference('TeamSlider.count', -1) do
      delete :destroy, id: @team_slider
    end

    assert_redirected_to team_sliders_path
  end
end
