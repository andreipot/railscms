require 'test_helper'

class WorkSlidersControllerTest < ActionController::TestCase
  setup do
    @work_slider = work_sliders(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:work_sliders)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create work_slider" do
    assert_difference('WorkSlider.count') do
      post :create, work_slider: { active_class: @work_slider.active_class, active_html: @work_slider.active_html, navigation_image: @work_slider.navigation_image, navigation_url: @work_slider.navigation_url, order: @work_slider.order, work_short_description: @work_slider.work_short_description }
    end

    assert_redirected_to work_slider_path(assigns(:work_slider))
  end

  test "should show work_slider" do
    get :show, id: @work_slider
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @work_slider
    assert_response :success
  end

  test "should update work_slider" do
    patch :update, id: @work_slider, work_slider: { active_class: @work_slider.active_class, active_html: @work_slider.active_html, navigation_image: @work_slider.navigation_image, navigation_url: @work_slider.navigation_url, order: @work_slider.order, work_short_description: @work_slider.work_short_description }
    assert_redirected_to work_slider_path(assigns(:work_slider))
  end

  test "should destroy work_slider" do
    assert_difference('WorkSlider.count', -1) do
      delete :destroy, id: @work_slider
    end

    assert_redirected_to work_sliders_path
  end
end
