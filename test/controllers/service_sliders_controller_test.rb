require 'test_helper'

class ServiceSlidersControllerTest < ActionController::TestCase
  setup do
    @service_slider = service_sliders(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:service_sliders)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create service_slider" do
    assert_difference('ServiceSlider.count') do
      post :create, service_slider: { active_class: @service_slider.active_class, active_html: @service_slider.active_html, navigation_image: @service_slider.navigation_image, order: @service_slider.order, service_short_description: @service_slider.service_short_description }
    end

    assert_redirected_to service_slider_path(assigns(:service_slider))
  end

  test "should show service_slider" do
    get :show, id: @service_slider
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @service_slider
    assert_response :success
  end

  test "should update service_slider" do
    patch :update, id: @service_slider, service_slider: { active_class: @service_slider.active_class, active_html: @service_slider.active_html, navigation_image: @service_slider.navigation_image, order: @service_slider.order, service_short_description: @service_slider.service_short_description }
    assert_redirected_to service_slider_path(assigns(:service_slider))
  end

  test "should destroy service_slider" do
    assert_difference('ServiceSlider.count', -1) do
      delete :destroy, id: @service_slider
    end

    assert_redirected_to service_sliders_path
  end
end
