require 'test_helper'

class WorksControllerTest < ActionController::TestCase
  setup do
    @work = works(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:works)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create work" do
    assert_difference('Work.count') do
      post :create, work: { detail_page_description: @work.detail_page_description, detail_page_link_text: @work.detail_page_link_text, detail_page_link_url: @work.detail_page_link_url, image: @work.image, list_page_description: @work.list_page_description, order: @work.order, short_link: @work.short_link, tags: @work.tags, title: @work.title }
    end

    assert_redirected_to work_path(assigns(:work))
  end

  test "should show work" do
    get :show, id: @work
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @work
    assert_response :success
  end

  test "should update work" do
    patch :update, id: @work, work: { detail_page_description: @work.detail_page_description, detail_page_link_text: @work.detail_page_link_text, detail_page_link_url: @work.detail_page_link_url, image: @work.image, list_page_description: @work.list_page_description, order: @work.order, short_link: @work.short_link, tags: @work.tags, title: @work.title }
    assert_redirected_to work_path(assigns(:work))
  end

  test "should destroy work" do
    assert_difference('Work.count', -1) do
      delete :destroy, id: @work
    end

    assert_redirected_to works_path
  end
end
