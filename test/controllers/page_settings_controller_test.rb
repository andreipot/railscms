require 'test_helper'

class PageSettingsControllerTest < ActionController::TestCase
  setup do
    @page_setting = page_settings(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:page_settings)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create page_setting" do
    assert_difference('PageSetting.count') do
      post :create, page_setting: {  }
    end

    assert_redirected_to page_setting_path(assigns(:page_setting))
  end

  test "should show page_setting" do
    get :show, id: @page_setting
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @page_setting
    assert_response :success
  end

  test "should update page_setting" do
    patch :update, id: @page_setting, page_setting: {  }
    assert_redirected_to page_setting_path(assigns(:page_setting))
  end

  test "should destroy page_setting" do
    assert_difference('PageSetting.count', -1) do
      delete :destroy, id: @page_setting
    end

    assert_redirected_to page_settings_path
  end
end
