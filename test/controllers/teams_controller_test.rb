require 'test_helper'

class TeamsControllerTest < ActionController::TestCase
  setup do
    @team = teams(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:teams)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create team" do
    assert_difference('Team.count') do
      post :create, team: { detail_page_bio: @team.detail_page_bio, detail_page_website_text: @team.detail_page_website_text, detail_page_website_url: @team.detail_page_website_url, dribble_url: @team.dribble_url, facebook_url: @team.facebook_url, github_url: @team.github_url, gplus_url: @team.gplus_url, image: @team.image, order: @team.order, position: @team.position, short_link: @team.short_link, team_page_bio: @team.team_page_bio, twitter_url: @team.twitter_url }
    end

    assert_redirected_to team_path(assigns(:team))
  end

  test "should show team" do
    get :show, id: @team
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @team
    assert_response :success
  end

  test "should update team" do
    patch :update, id: @team, team: { detail_page_bio: @team.detail_page_bio, detail_page_website_text: @team.detail_page_website_text, detail_page_website_url: @team.detail_page_website_url, dribble_url: @team.dribble_url, facebook_url: @team.facebook_url, github_url: @team.github_url, gplus_url: @team.gplus_url, image: @team.image, order: @team.order, position: @team.position, short_link: @team.short_link, team_page_bio: @team.team_page_bio, twitter_url: @team.twitter_url }
    assert_redirected_to team_path(assigns(:team))
  end

  test "should destroy team" do
    assert_difference('Team.count', -1) do
      delete :destroy, id: @team
    end

    assert_redirected_to teams_path
  end
end
