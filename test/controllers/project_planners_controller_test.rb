require 'test_helper'

class ProjectPlannersControllerTest < ActionController::TestCase
  setup do
    @project_planner = project_planners(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:project_planners)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create project_planner" do
    assert_difference('ProjectPlanner.count') do
      post :create, project_planner: { budget: @project_planner.budget, company: @project_planner.company, email_address: @project_planner.email_address, full_name: @project_planner.full_name, hear_about_us: @project_planner.hear_about_us, launch_date: @project_planner.launch_date, main_objective: @project_planner.main_objective, phone_number: @project_planner.phone_number, project_url: @project_planner.project_url, schedule_considerations: @project_planner.schedule_considerations, site_concept: @project_planner.site_concept }
    end

    assert_redirected_to project_planner_path(assigns(:project_planner))
  end

  test "should show project_planner" do
    get :show, id: @project_planner
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @project_planner
    assert_response :success
  end

  test "should update project_planner" do
    patch :update, id: @project_planner, project_planner: { budget: @project_planner.budget, company: @project_planner.company, email_address: @project_planner.email_address, full_name: @project_planner.full_name, hear_about_us: @project_planner.hear_about_us, launch_date: @project_planner.launch_date, main_objective: @project_planner.main_objective, phone_number: @project_planner.phone_number, project_url: @project_planner.project_url, schedule_considerations: @project_planner.schedule_considerations, site_concept: @project_planner.site_concept }
    assert_redirected_to project_planner_path(assigns(:project_planner))
  end

  test "should destroy project_planner" do
    assert_difference('ProjectPlanner.count', -1) do
      delete :destroy, id: @project_planner
    end

    assert_redirected_to project_planners_path
  end
end
