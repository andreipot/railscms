require 'test_helper'

class ServicesControllerTest < ActionController::TestCase
  setup do
    @service = services(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:services)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create service" do
    assert_difference('Service.count') do
      post :create, service: { detail_first_heading: @service.detail_first_heading, detail_first_paragraph: @service.detail_first_paragraph, detail_icon_css_class: @service.detail_icon_css_class, detail_second_heading: @service.detail_second_heading, detail_second_paragraph: @service.detail_second_paragraph, detail_third_heading: @service.detail_third_heading, detail_third_paragrah: @service.detail_third_paragrah, detail_top_description: @service.detail_top_description, homepage_description: @service.homepage_description, homepage_icon: @service.homepage_icon, homepage_icon_hover: @service.homepage_icon_hover, name: @service.name, order: @service.order, short_link: @service.short_link }
    end

    assert_redirected_to service_path(assigns(:service))
  end

  test "should show service" do
    get :show, id: @service
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @service
    assert_response :success
  end

  test "should update service" do
    patch :update, id: @service, service: { detail_first_heading: @service.detail_first_heading, detail_first_paragraph: @service.detail_first_paragraph, detail_icon_css_class: @service.detail_icon_css_class, detail_second_heading: @service.detail_second_heading, detail_second_paragraph: @service.detail_second_paragraph, detail_third_heading: @service.detail_third_heading, detail_third_paragrah: @service.detail_third_paragrah, detail_top_description: @service.detail_top_description, homepage_description: @service.homepage_description, homepage_icon: @service.homepage_icon, homepage_icon_hover: @service.homepage_icon_hover, name: @service.name, order: @service.order, short_link: @service.short_link }
    assert_redirected_to service_path(assigns(:service))
  end

  test "should destroy service" do
    assert_difference('Service.count', -1) do
      delete :destroy, id: @service
    end

    assert_redirected_to services_path
  end
end
