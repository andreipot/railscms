require 'test_helper'

class HomeSlidersControllerTest < ActionController::TestCase
  setup do
    @home_slider = home_sliders(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:home_sliders)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create home_slider" do
    assert_difference('HomeSlider.count') do
      post :create, home_slider: { description: @home_slider.description, image: @home_slider.image, layout: @home_slider.layout, link: @home_slider.link, tags: @home_slider.tags, title: @home_slider.title }
    end

    assert_redirected_to home_slider_path(assigns(:home_slider))
  end

  test "should show home_slider" do
    get :show, id: @home_slider
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @home_slider
    assert_response :success
  end

  test "should update home_slider" do
    patch :update, id: @home_slider, home_slider: { description: @home_slider.description, image: @home_slider.image, layout: @home_slider.layout, link: @home_slider.link, tags: @home_slider.tags, title: @home_slider.title }
    assert_redirected_to home_slider_path(assigns(:home_slider))
  end

  test "should destroy home_slider" do
    assert_difference('HomeSlider.count', -1) do
      delete :destroy, id: @home_slider
    end

    assert_redirected_to home_sliders_path
  end
end
