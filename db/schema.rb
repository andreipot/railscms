# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20150401201735) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "active_admin_comments", force: true do |t|
    t.string   "namespace"
    t.text     "body"
    t.string   "resource_id",   null: false
    t.string   "resource_type", null: false
    t.integer  "author_id"
    t.string   "author_type"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "active_admin_comments", ["author_type", "author_id"], name: "index_active_admin_comments_on_author_type_and_author_id", using: :btree
  add_index "active_admin_comments", ["namespace"], name: "index_active_admin_comments_on_namespace", using: :btree
  add_index "active_admin_comments", ["resource_type", "resource_id"], name: "index_active_admin_comments_on_resource_type_and_resource_id", using: :btree

  create_table "admin_users", force: true do |t|
    t.string   "email",                  default: "", null: false
    t.string   "encrypted_password",     default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.inet     "current_sign_in_ip"
    t.inet     "last_sign_in_ip"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "admin_users", ["email"], name: "index_admin_users_on_email", unique: true, using: :btree
  add_index "admin_users", ["reset_password_token"], name: "index_admin_users_on_reset_password_token", unique: true, using: :btree

  create_table "contacts", force: true do |t|
    t.string   "email"
    t.string   "full_name"
    t.string   "company"
    t.string   "subject"
    t.text     "message"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "home_sliders", force: true do |t|
    t.string   "layout"
    t.string   "title"
    t.text     "description"
    t.string   "link"
    t.string   "tags"
    t.string   "image_file_name"
    t.string   "image_content_type"
    t.integer  "image_file_size"
    t.datetime "image_updated_at"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "order"
  end

  create_table "page_meta_infos", force: true do |t|
    t.string   "uri"
    t.string   "keywords"
    t.string   "description"
    t.string   "title"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "page_settings", force: true do |t|
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "project_planners", force: true do |t|
    t.string   "full_name"
    t.string   "company"
    t.string   "phone_number"
    t.string   "email_address"
    t.string   "hear_about_us"
    t.string   "project_url"
    t.string   "launch_date"
    t.integer  "budget"
    t.text     "site_concept"
    t.text     "main_objective"
    t.text     "schedule_considerations"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "service_sliders", force: true do |t|
    t.string   "service_short_description"
    t.integer  "order"
    t.string   "active_class"
    t.text     "active_html"
    t.string   "navigation_image_file_name"
    t.string   "navigation_image_content_type"
    t.integer  "navigation_image_file_size"
    t.datetime "navigation_image_updated_at"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "service_id"
  end

  add_index "service_sliders", ["service_id"], name: "index_service_sliders_on_service_id", using: :btree

  create_table "services", force: true do |t|
    t.string   "short_link"
    t.string   "name"
    t.integer  "order"
    t.string   "homepage_icon_file_name"
    t.string   "homepage_icon_content_type"
    t.integer  "homepage_icon_file_size"
    t.datetime "homepage_icon_updated_at"
    t.string   "homepage_icon_hover_file_name"
    t.string   "homepage_icon_hover_content_type"
    t.integer  "homepage_icon_hover_file_size"
    t.datetime "homepage_icon_hover_updated_at"
    t.string   "homepage_description"
    t.string   "detail_top_description"
    t.string   "detail_icon_css_class"
    t.string   "detail_first_heading"
    t.string   "detail_first_paragraph"
    t.string   "detail_second_heading"
    t.string   "detail_second_paragraph"
    t.string   "detail_third_heading"
    t.string   "detail_third_paragrah"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "site_configs", force: true do |t|
    t.string   "key"
    t.string   "value"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "team_sliders", force: true do |t|
    t.string   "team_member_short_link"
    t.integer  "order"
    t.string   "image_file_name"
    t.string   "image_content_type"
    t.integer  "image_file_size"
    t.datetime "image_updated_at"
    t.string   "title"
    t.string   "work_short_link"
    t.string   "website_text"
    t.string   "website_link"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "team_id"
  end

  add_index "team_sliders", ["team_id"], name: "index_team_sliders_on_team_id", using: :btree

  create_table "teams", force: true do |t|
    t.string   "short_link"
    t.string   "position"
    t.integer  "order"
    t.string   "image_file_name"
    t.string   "image_content_type"
    t.integer  "image_file_size"
    t.datetime "image_updated_at"
    t.string   "team_page_bio"
    t.string   "detail_page_website_text"
    t.string   "detail_page_website_url"
    t.text     "detail_page_bio"
    t.string   "facebook_url"
    t.string   "twitter_url"
    t.string   "gplus_url"
    t.string   "dribble_url"
    t.string   "github_url"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "users", force: true do |t|
    t.string   "email",                  default: "", null: false
    t.string   "encrypted_password",     default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.inet     "current_sign_in_ip"
    t.inet     "last_sign_in_ip"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "users", ["email"], name: "index_users_on_email", unique: true, using: :btree
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree

  create_table "work_sliders", force: true do |t|
    t.string   "work_short_link"
    t.integer  "order"
    t.string   "navigation_image_file_name"
    t.string   "navigation_image_content_type"
    t.integer  "navigation_image_file_size"
    t.datetime "navigation_image_updated_at"
    t.string   "navigation_url"
    t.string   "active_class"
    t.text     "active_html"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "work_id"
  end

  add_index "work_sliders", ["work_id"], name: "index_work_sliders_on_work_id", using: :btree

  create_table "works", force: true do |t|
    t.string   "short_link"
    t.string   "title"
    t.string   "tags"
    t.integer  "order"
    t.text     "list_page_description"
    t.string   "detail_page_link_text"
    t.string   "detail_page_link_url"
    t.text     "detail_page_description"
    t.string   "image_file_name"
    t.string   "image_content_type"
    t.integer  "image_file_size"
    t.datetime "image_updated_at"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

end
