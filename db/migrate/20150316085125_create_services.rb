class CreateServices < ActiveRecord::Migration
  def change
    create_table :services do |t|
      t.string :short_link
      t.string :name
      t.integer :order
      t.attachment :homepage_icon
      t.attachment :homepage_icon_hover
      t.string :homepage_description
      t.string :detail_top_description
      t.string :detail_icon_css_class
      t.string :detail_first_heading
      t.string :detail_first_paragraph
      t.string :detail_second_heading
      t.string :detail_second_paragraph
      t.string :detail_third_heading
      t.string :detail_third_paragrah

      t.timestamps
    end
  end
end
