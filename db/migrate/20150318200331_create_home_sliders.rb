class CreateHomeSliders < ActiveRecord::Migration
  def change
    create_table :home_sliders do |t|
      t.string :layout
      t.string :title
      t.text :description
      t.string :link
      t.string :tags
      t.attachment :image

      t.timestamps
    end
  end
end
