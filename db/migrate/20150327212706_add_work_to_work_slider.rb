class AddWorkToWorkSlider < ActiveRecord::Migration
  def change
    add_reference :work_sliders, :work, index: true
  end
end
