class ChangeWorkSliderFieldName < ActiveRecord::Migration
  def change
    rename_column :work_sliders, :work_short_description, :work_short_link
  end
end
