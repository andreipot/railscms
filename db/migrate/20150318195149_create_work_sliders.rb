class CreateWorkSliders < ActiveRecord::Migration
  def change
    create_table :work_sliders do |t|
      t.string :work_short_description
      t.integer :order
      t.attachment :navigation_image
      t.string :navigation_url
      t.string :active_class
      t.text :active_html
      t.timestamps
    end
  end
end
