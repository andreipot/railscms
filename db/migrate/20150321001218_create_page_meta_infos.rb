class CreatePageMetaInfos < ActiveRecord::Migration
  def change
    create_table :page_meta_infos do |t|
      t.string :uri
      t.string :keywords
      t.string :description
      t.string :title

      t.timestamps
    end
  end
end
