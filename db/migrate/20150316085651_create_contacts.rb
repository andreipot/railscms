class CreateContacts < ActiveRecord::Migration
  def change
    create_table :contacts do |t|
      t.string :email
      t.string :full_name
      t.string :company
      t.string :subject
      t.text :message

      t.timestamps
    end
  end
end
