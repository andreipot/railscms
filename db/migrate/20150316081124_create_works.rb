class CreateWorks < ActiveRecord::Migration
  def change
    create_table :works do |t|
      t.string :short_link
      t.string :title
      t.string :tags
      t.integer :order
      t.string :list_page_description
      t.string :detail_page_link_text
      t.string :detail_page_link_url
      t.string :detail_page_description
      t.attachment :image

      t.timestamps
    end
  end
end
