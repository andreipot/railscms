class ServiceSliderColumnChange < ActiveRecord::Migration
  def change
    change_column :service_sliders, :active_html, :text
  end
end
