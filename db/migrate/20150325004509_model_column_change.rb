class ModelColumnChange < ActiveRecord::Migration
  def change
    add_column    :home_sliders, :order, :integer ,:required=> true
    change_column :project_planners, :site_concept, :text
    change_column :project_planners, :main_objective, :text
    change_column :project_planners, :schedule_considerations, :text
    change_column :team_sliders, :website_text, :string
    change_column :works, :list_page_description, :text
    change_column :works, :detail_page_description, :text
  end
end
