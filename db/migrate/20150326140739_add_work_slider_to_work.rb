class AddWorkSliderToWork < ActiveRecord::Migration
  def change
    add_reference :works, :work_slider, index: true
  end
end
