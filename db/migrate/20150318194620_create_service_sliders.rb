class CreateServiceSliders < ActiveRecord::Migration
  def change
    create_table :service_sliders do |t|
      t.string :service_short_description
      t.integer :order
      t.string :active_class
      t.string :active_html
      t.attachment :navigation_image

      t.timestamps
    end
  end
end
