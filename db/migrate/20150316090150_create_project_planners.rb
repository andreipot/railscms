class CreateProjectPlanners < ActiveRecord::Migration
  def change
    create_table :project_planners do |t|
      t.string :full_name
      t.string :company
      t.string :phone_number
      t.string :email_address
      t.string :hear_about_us
      t.string :project_url
      t.string :launch_date
      t.integer :budget
      t.string :site_concept
      t.string :main_objective
      t.string :schedule_considerations

      t.timestamps
    end
  end
end
