class AddServiceToServiceSlider < ActiveRecord::Migration
  def change
    add_reference :service_sliders, :service, index: true
  end
end
