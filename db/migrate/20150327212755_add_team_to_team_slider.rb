class AddTeamToTeamSlider < ActiveRecord::Migration
  def change
    add_reference :team_sliders, :team, index: true
  end
end
