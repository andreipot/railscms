class CreateTeamSliders < ActiveRecord::Migration
  def change
    create_table :team_sliders do |t|
      t.string :team_member_short_link
      t.integer :order
      t.attachment :image
      t.string :title
      t.string :work_short_link
      t.text :website_text
      t.string :website_link

      t.timestamps
    end
  end
end
