class CreateTeams < ActiveRecord::Migration
  def change
    create_table :teams do |t|
      t.string :short_link
      t.string :position
      t.integer :order
      t.attachment :image
      t.string :team_page_bio
      t.string :detail_page_website_text
      t.string :detail_page_website_url
      t.string :detail_page_bio
      t.string :facebook_url
      t.string :twitter_url
      t.string :gplus_url
      t.string :dribble_url
      t.string :github_url

      t.timestamps
    end
  end
end
