(function() {
    $(document).ready(function() {
        var s_DetailGallery = $('#work-detail-gallery');
        var s_ActiveViewport = s_DetailGallery.find('.gallery-active-viewport');
        var s_ActiveDisplayItems = s_ActiveViewport.find('.gallery-active-viewport-item');
        var s_Navigation = s_DetailGallery.find('.gallery-navigation');
        var s_NavigationViewport = s_Navigation.find('.gallery-navigation-viewport');
        var s_NavigationItems = s_NavigationViewport.find('.gallery-navigation-items');
        var s_Items = s_NavigationItems.find('.gallery-navigation-item');
        var s_ItemCount = s_Items.length;
        var s_TopFade = s_NavigationViewport.find('.gallery-navigation-top-fade');
        var s_BottomFade = s_NavigationViewport.find('.gallery-navigation-bottom-fade');
        var s_LeftFade = s_NavigationViewport.find('.gallery-navigation-left-fade');
        var s_RightFade = s_NavigationViewport.find('.gallery-navigation-right-fade');
        var s_PreviousButton = s_Navigation.find('.gallery-navigation-previous');
        var s_NextButton = s_Navigation.find('.gallery-navigation-next');
        var s_PreviousContainer = s_Navigation.find('.gallery-navigation-previous-container');
        var s_NextContainer = s_Navigation.find('.gallery-navigation-next-container');
        var s_VerticalNavigationReservedSpace = 124;
        var s_HorizontalNavigationReservedSpace = 124;

        /**
         * Get the index of the item that is first in line when displaying the navigation.
         * This is not the active item!
         * @return {number}
         */
        var GetCurrentNavigationIndex = function()
        {
            var s_CurrentIndex = 0;
            var s_Counter = 0;

            s_Items.each(function() {
                var s_Item = $(this);

                if(s_Item.hasClass('current')) {
                    s_CurrentIndex = s_Counter;
                }

                s_Counter++;
            });

            return s_CurrentIndex;
        };

        /**
         * Set the index of the item that is first in line when displaying the navigation.
         * @param index
         */
        var SetCurrentNavigationIndex = function(index)
        {
            s_Items.removeClass('current');

            s_Items.eq(index).addClass('current');
        };

        /**
         * Will return 'vertical' when in the vertical navigation state, and 'horizontal' otherwise.
         * @returns {string}
         */
        var GetDisplayMode = function()
        {
            if(s_Navigation.css('float') == 'right')
            {
                return 'vertical';
            }

            if(s_PreviousContainer.css('display') == 'none')
            {
                return 'horizontal_small';
            }

            return 'horizontal';
        };

        /**
         * Update the vertical top positioning offset for the navigation items.
         */
        var UpdateVerticalOffset = function()
        {
            var s_CurrentIndex = GetCurrentNavigationIndex();

            var s_ItemHeight = s_Items.eq(0).height();
            var s_ItemMargin = parseInt(s_Items.eq(0).css('margin-bottom'), 10);

            var s_NewTop = (s_CurrentIndex * (s_ItemHeight + s_ItemMargin) * -1) + 'px';

            s_NavigationItems.css('top', s_NewTop).css('left', '0px');
        };

        /**
         * Update the horizontal left positioning offset for the navigation items.
         */
        var UpdateHorizontalOffset = function()
        {
            var s_CurrentIndex = GetCurrentNavigationIndex();

            var s_ItemWidth = s_Items.eq(0).width();
            var s_ItemMargin = parseInt(s_Items.eq(0).css('margin-right'), 10);

            var s_NewLeft = (s_CurrentIndex * (s_ItemWidth + s_ItemMargin) * -1) + 'px';

            s_NavigationItems.css('left', s_NewLeft).css('top', '0px');
        };

        /**
         * Calculate how many items vertically (full items, halfs are not counted) that are visible in the current view.
         * @return {number}
         */
        var CalculateMaxVisibleVerticalItems = function()
        {
            var s_ActiveViewportHeight = s_ActiveViewport.height();
            var s_VisibleHeight = s_ActiveViewportHeight - s_VerticalNavigationReservedSpace;

            var s_ItemHeight = s_Items.eq(0).height();
            var s_ItemMargin = parseInt(s_Items.eq(0).css('margin-bottom'), 10);

            var s_CurrentlyVisible = 0;

            for(var s_ItemIndex = 0; s_ItemIndex < s_ItemCount; s_ItemIndex++)
            {
                s_VisibleHeight -= s_ItemHeight;

                if(s_VisibleHeight < 0)
                {
                    break;
                }

                s_CurrentlyVisible++;

                s_VisibleHeight -= s_ItemMargin;
            }

            return s_CurrentlyVisible;
        };

        /**
         * Calculate how many items horizontally (full items, halfs are not counted) that are visible in the current view.
         * @return {number}
         */
        var CalculateMaxVisibleHorizontalItems = function()
        {
            var s_VisibleWidth = s_Navigation.outerWidth() - s_HorizontalNavigationReservedSpace;

            var s_ItemWidth = s_Items.eq(0).width();
            var s_ItemMargin = parseInt(s_Items.eq(0).css('margin-right'), 10);

            var s_CurrentlyVisible = 0;

            for(var s_ItemIndex = 0; s_ItemIndex < s_ItemCount; s_ItemIndex++)
            {
                s_VisibleWidth -= s_ItemWidth;

                if(s_VisibleWidth < 0)
                {
                    break;
                }

                s_CurrentlyVisible++;

                s_VisibleWidth -= s_ItemMargin;
            }

            return s_CurrentlyVisible;
        };

        /**
         * Hide / show fades and buttons for the vertical state.
         */
        var UpdateVerticalMisc = function()
        {
            var s_VisibleItemCount = CalculateMaxVisibleVerticalItems();
            var s_CurrentIndex = GetCurrentNavigationIndex();
            var s_LastVisibleItemIndex = s_CurrentIndex + s_VisibleItemCount - 1;

            if(s_CurrentIndex == 0)
            {
                s_TopFade.addClass('hide-fade');
                s_PreviousButton.addClass('hide-button');
            }
            else
            {
                s_TopFade.removeClass('hide-fade');
                s_PreviousButton.removeClass('hide-button');
            }

            if(s_LastVisibleItemIndex >= (s_ItemCount - 1))
            {
                s_BottomFade.addClass('hide-fade');
                s_NextButton.addClass('hide-button');
            }
            else
            {
                s_BottomFade.removeClass('hide-fade');
                s_NextButton.removeClass('hide-button');
            }
        };

        /**
         * Hide / show fades and buttons for the horizontal state.
         */
        var UpdateHorizontalMisc = function()
        {
            var s_VisibleItemCount = CalculateMaxVisibleHorizontalItems();
            var s_CurrentIndex = GetCurrentNavigationIndex();
            var s_LastVisibleItemIndex = s_CurrentIndex + s_VisibleItemCount - 1;

            if(s_CurrentIndex == 0)
            {
                s_LeftFade.addClass('hide-fade');
                s_PreviousButton.addClass('hide-button');
            }
            else
            {
                s_LeftFade.removeClass('hide-fade');
                s_PreviousButton.removeClass('hide-button');
            }

            if(s_LastVisibleItemIndex >= (s_ItemCount - 1))
            {
                s_RightFade.addClass('hide-fade');
                s_NextButton.addClass('hide-button');
            }
            else
            {
                s_RightFade.removeClass('hide-fade');
                s_NextButton.removeClass('hide-button');
            }
        };

        var OnVerticalThink = function() {
            // Reset the horizontal changes to the width of the viewport
            s_NavigationViewport.css('width', 'auto');

            // First fix the height of the item viewport
            var s_ActiveViewportHeight = s_ActiveViewport.height();
            var s_NewHeight = s_ActiveViewportHeight - s_VerticalNavigationReservedSpace;

            s_NavigationViewport.height(s_NewHeight);

            // Next fix the offset for the 'current' item (ie: item that is first in line)
            UpdateVerticalOffset();

            // We need to update the buttons AND show / hide the top / bottom fade
            UpdateVerticalMisc();
        };

        var OnHorizontalThink = function()
        {
            // Reset the vertical changes to the height of the viewport
            s_NavigationViewport.css('height', null);

            // Fix the width of the item viewport
            var s_NewWidth = s_Navigation.outerWidth() - s_HorizontalNavigationReservedSpace;

            s_NavigationViewport.width(s_NewWidth);

            // Next fix the offset for the 'current' item (ie: item that is first in line)
            UpdateHorizontalOffset();

            // We need to update the buttons AND show / hide the left / right fade
            UpdateHorizontalMisc();
        };

        var OnHorizontalSmallThink = function()
        {
            // Reset the vertical changes to the height of the viewport
            s_NavigationViewport.css('height', 'auto');

            // Reset the horizontal changes to the width of the viewport
            s_NavigationViewport.css('width', 'auto');

            // Reset the top / left for the items
            s_Items.css('left', '0px', 'top', '0px');
        };

        var OnThink = function()
        {
            var s_DisplayMode = GetDisplayMode();

            if(s_DisplayMode == 'vertical')
                OnVerticalThink();
            else if(s_DisplayMode == 'horizontal')
                OnHorizontalThink();
            else if(s_DisplayMode == 'horizontal_small')
                OnHorizontalSmallThink();
        };

        var OnPreviousButton = function()
        {
            var s_DisplayMode = GetDisplayMode();
            var s_CurrentNavigationIndex = GetCurrentNavigationIndex();

            if(s_CurrentNavigationIndex <= 0)
            {
                return;
            }

            SetCurrentNavigationIndex(s_CurrentNavigationIndex - 1);

            if(s_DisplayMode == 'vertical')
            {
                // Fix the offset for the 'current' item (ie: item that is first in line)
                UpdateVerticalOffset();

                // We need to update the buttons AND show / hide the top / bottom fade
                UpdateVerticalMisc();
            }
            else if(s_DisplayMode == 'horizontal')
            {
                // Fix the offset for the 'current' item (ie: item that is first in line)
                UpdateHorizontalOffset();

                // We need to update the buttons AND show / hide the top / bottom fade
                UpdateHorizontalMisc();
            }
        };

        var OnNextButton = function()
        {
            var s_DisplayMode = GetDisplayMode();

            var s_CurrentNavigationIndex = GetCurrentNavigationIndex();
            var s_VisibleItemCount = s_DisplayMode == 'vertical' ? CalculateMaxVisibleVerticalItems() : CalculateMaxVisibleHorizontalItems();
            var s_LastVisibleItemIndex = s_CurrentNavigationIndex + s_VisibleItemCount - 1;

            if(s_LastVisibleItemIndex >= (s_ItemCount - 1))
            {
                return;
            }

            SetCurrentNavigationIndex(s_CurrentNavigationIndex + 1);

            if(s_DisplayMode == 'vertical')
            {
                // Fix the offset for the 'current' item (ie: item that is first in line)
                UpdateVerticalOffset();

                // We need to update the buttons AND show / hide the left / right fade
                UpdateVerticalMisc();
            }
            else if(s_DisplayMode == 'horizontal')
            {
                // Fix the offset for the 'current' item (ie: item that is first in line)
                UpdateHorizontalOffset();

                // We need to update the buttons AND show / hide the left / right fade
                UpdateHorizontalMisc();
            }
        };

        s_PreviousButton.click(function() {
            OnPreviousButton();

            return false;
        });

        s_NextButton.click(function() {
            OnNextButton();

            return false;
        });



        var s_TempIndex = 0;

        s_Items.each(function() {
            var s_Item = $(this);

            s_Item.attr('data-index', s_TempIndex);

            s_TempIndex++;
        });

        s_TempIndex = 0;

        s_ActiveDisplayItems.each(function() {
            var s_Item = $(this);

            s_Item.attr('data-index', s_TempIndex);

            s_TempIndex++;
        });

        s_Items.click(function() {
            var s_NaviItem = $(this);

            if(s_NaviItem.attr('href') != '#') {
                return;
            }

            var s_NaviIndex = s_NaviItem.attr('data-index');

            s_ActiveDisplayItems.removeClass('active');

            s_ActiveDisplayItems.filter('[data-index="' + s_NaviIndex + '"]').addClass('active');

            return false;
        });



        $(window).resize(function() {
            OnThink();
        });

        OnThink();

        setTimeout(function() {
            OnThink();
        }, 2000);
    });
})();