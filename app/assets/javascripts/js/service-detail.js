(function() {
    $(document).ready(function() {
        var s_SliderColumn = $('#service-detail-split .slider-column');
        var s_ActiveDisplay = s_SliderColumn.find('.active-display');
        var s_ActiveDisplayItems = s_ActiveDisplay.find('.active-display-item');
        var s_Navigation = s_SliderColumn.find('.slider-navigation');
        var s_NavigationViewport = s_Navigation.find('.navigation-viewport');
        var s_NavigationItems = s_NavigationViewport.find('.navigation-items');
        var s_Items = s_NavigationItems.find('.navigation-item');
        var s_ItemCount = s_Items.length;
        var s_LeftFade = s_NavigationViewport.find('.navigation-left-fade');
        var s_RightFade = s_NavigationViewport.find('.navigation-right-fade');
        var s_PreviousButton = s_Navigation.find('.navigation-previous');
        var s_NextButton = s_Navigation.find('.navigation-next');
        var s_PreviousContainer = s_Navigation.find('.navigation-previous-container');
        var s_NextContainer = s_Navigation.find('.navigation-next-container');
        var s_HorizontalNavigationReservedSpace = 54;
        var s_MoreServices = $('#service-detail-top .more-services');
        var s_RelatedBlogBox = $('#service-detail-bottom .related-blog-box');
        var s_RelatedPosts = s_RelatedBlogBox.find('.related-post');
        var s_RelatedPostReservedSpace = 90;
        var s_FontSizeToAvgWidthFactor = 0.51;

        /**
         * Update the display text (truncated if needed) for the related blog box
         */
        var OnTruncateThink = function() {
            s_RelatedPosts.each(function() {
                var s_RelatedPost = $(this);

                var s_PostLink = s_RelatedPost.find('.post-link');
                var s_FullText = $.trim(s_PostLink.attr('data-text'));

                var s_TextContainer = s_PostLink.find('span');
                var s_AverageLetterWidth = parseInt(s_TextContainer.css('font-size'), 10) * s_FontSizeToAvgWidthFactor;

                var s_MaxTextWidth = s_RelatedPost.width() - s_RelatedPostReservedSpace;

                if(s_FullText.length * s_AverageLetterWidth <= s_MaxTextWidth)
                {
                    s_TextContainer.text(s_FullText);
                    return;
                }

                var s_MaxLetterCount = Math.floor(s_MaxTextWidth / s_AverageLetterWidth);
                var s_NewText = $.trim(s_FullText.substring(0, s_MaxLetterCount - 3)) + '...';

                s_TextContainer.text(s_NewText);
            });
        };

        /**
         * Get the index of the item that is first in line when displaying the navigation.
         * This is not the active item!
         * @return {number}
         */
        var GetCurrentNavigationIndex = function()
        {
            var s_CurrentIndex = 0;
            var s_Counter = 0;

            s_Items.each(function() {
                var s_Item = $(this);

                if(s_Item.hasClass('current')) {
                    s_CurrentIndex = s_Counter;
                }

                s_Counter++;
            });

            return s_CurrentIndex;
        };

        /**
         * Set the index of the item that is first in line when displaying the navigation.
         * @param index
         */
        var SetCurrentNavigationIndex = function(index)
        {
            s_Items.removeClass('current');

            s_Items.eq(index).addClass('current');
        };

        /**
         * Update the horizontal left positioning offset for the navigation items.
         */
        var UpdateHorizontalOffset = function()
        {
            var s_CurrentIndex = GetCurrentNavigationIndex();

            var s_ItemWidth = s_Items.eq(0).width();
            var s_ItemMargin = parseInt(s_Items.eq(0).css('margin-right'), 10);

            var s_NewLeft = (s_CurrentIndex * (s_ItemWidth + s_ItemMargin) * -1) + 'px';

            s_NavigationItems.css('left', s_NewLeft).css('top', '0px');
        };

        /**
         * Calculate how many items horizontally (full items, halfs are not counted) that are visible in the current view.
         * @return {number}
         */
        var CalculateMaxVisibleHorizontalItems = function()
        {
            var s_VisibleWidth = s_Navigation.outerWidth() - s_HorizontalNavigationReservedSpace;

            var s_ItemWidth = s_Items.eq(0).width();
            var s_ItemMargin = parseInt(s_Items.eq(0).css('margin-right'), 10);

            var s_CurrentlyVisible = 0;

            for(var s_ItemIndex = 0; s_ItemIndex < s_ItemCount; s_ItemIndex++)
            {
                s_VisibleWidth -= s_ItemWidth;

                if(s_VisibleWidth < 0)
                {
                    break;
                }

                s_CurrentlyVisible++;

                s_VisibleWidth -= s_ItemMargin;
            }

            return s_CurrentlyVisible;
        };

        /**
         * Hide / show fades and buttons for the horizontal state.
         */
        var UpdateHorizontalMisc = function()
        {
            var s_VisibleItemCount = CalculateMaxVisibleHorizontalItems();
            var s_CurrentIndex = GetCurrentNavigationIndex();
            var s_LastVisibleItemIndex = s_CurrentIndex + s_VisibleItemCount - 1;

            if(s_CurrentIndex == 0)
            {
                s_LeftFade.addClass('hide-fade');
                s_PreviousButton.addClass('hide-button');
            }
            else
            {
                s_LeftFade.removeClass('hide-fade');
                s_PreviousButton.removeClass('hide-button');
            }

            if(s_LastVisibleItemIndex >= (s_ItemCount - 1))
            {
                s_RightFade.addClass('hide-fade');
                s_NextButton.addClass('hide-button');
            }
            else
            {
                s_RightFade.removeClass('hide-fade');
                s_NextButton.removeClass('hide-button');
            }
        };

        var OnSliderThink = function()
        {
            // Fix the width of the item viewport
            var s_NewWidth = s_Navigation.outerWidth() - s_HorizontalNavigationReservedSpace;

            s_NavigationViewport.width(s_NewWidth);

            // Next fix the offset for the 'current' item (ie: item that is first in line)
            UpdateHorizontalOffset();

            // We need to update the buttons AND show / hide the left / right fade
            UpdateHorizontalMisc();
        };

        var OnPreviousButton = function()
        {
            var s_CurrentNavigationIndex = GetCurrentNavigationIndex();

            if(s_CurrentNavigationIndex <= 0)
            {
                return;
            }

            SetCurrentNavigationIndex(s_CurrentNavigationIndex - 1);

            // Fix the offset for the 'current' item (ie: item that is first in line)
            UpdateHorizontalOffset();

            // We need to update the buttons AND show / hide the top / bottom fade
            UpdateHorizontalMisc();
        };

        var OnNextButton = function()
        {
            var s_CurrentNavigationIndex = GetCurrentNavigationIndex();
            var s_VisibleItemCount = CalculateMaxVisibleHorizontalItems();
            var s_LastVisibleItemIndex = s_CurrentNavigationIndex + s_VisibleItemCount - 1;

            if(s_LastVisibleItemIndex >= (s_ItemCount - 1))
            {
                return;
            }

            SetCurrentNavigationIndex(s_CurrentNavigationIndex + 1);

            // Fix the offset for the 'current' item (ie: item that is first in line)
            UpdateHorizontalOffset();

            // We need to update the buttons AND show / hide the left / right fade
            UpdateHorizontalMisc();
        };

        s_PreviousButton.click(function() {
            OnPreviousButton();

            return false;
        });

        s_NextButton.click(function() {
            OnNextButton();

            return false;
        });

        var s_TempIndex = 0;

        s_Items.each(function() {
            var s_Item = $(this);

            s_Item.attr('data-index', s_TempIndex);

            s_TempIndex++;
        });

        s_TempIndex = 0;

        s_ActiveDisplayItems.each(function() {
            var s_Item = $(this);

            s_Item.attr('data-index', s_TempIndex);

            s_TempIndex++;
        });

        s_Items.click(function() {
            var s_NaviItem = $(this);

            var s_NaviIndex = s_NaviItem.attr('data-index');

            s_ActiveDisplayItems.removeClass('active');

            s_ActiveDisplayItems.filter('[data-index="' + s_NaviIndex + '"]').addClass('active');

            return false;
        });

        $(window).resize(function() {
            OnSliderThink();
            OnTruncateThink();
        });

        OnSliderThink();
        OnTruncateThink();

        setTimeout(function() {
            OnSliderThink();
        }, 2000);

        var CloseDropdown = function() {
            s_MoreServices.removeClass('state-opened').addClass('state-closed');
        };

        var OpenDropdown = function() {
            s_MoreServices.removeClass('state-closed').addClass('state-opened');
        };

        $(document).click(function(e) {
            var s_ClickTarget = $(e.target);

            if(!s_ClickTarget.hasClass('dropdown-button') && !s_ClickTarget.hasClass('dropdown-item')) {
                if(s_MoreServices.hasClass('state-opened')) {
                    CloseDropdown();
                }

                return;
            }

            if(s_ClickTarget.hasClass('dropdown-item')) {
                CloseDropdown();
                return;
            }

            if(s_MoreServices.hasClass('state-closed')) {
                OpenDropdown();
            }
            else {
                CloseDropdown();
            }

            return false;
        });

        $('a#submitForm').click(function(){
            $('form#new_contact').submit();
        })

    });
})();