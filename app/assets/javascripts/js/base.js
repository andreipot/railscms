(function() {
    $(document).ready(function() {
        $('#mobile-menu').click(function() {
            var header = $('header');

            if(header.hasClass('state-opened')) {
                header.removeClass('state-opened');
                header.addClass('state-closed');
            }
            else {
                header.removeClass('state-closed');
                header.addClass('state-opened');
            }

            return false;
        });
    });
})();