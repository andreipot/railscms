(function() {
    $(document).ready(function() {
        //var s_ContactForm = $('#contact-area .contact-box form');
        var s_ContactForm = $('form#new_contact');
        var s_NameField = s_ContactForm.find('input[name="contact[full_name]"]');
        var s_CompanyField = s_ContactForm.find('input[name="contact[company]"]');
        var s_EmailField = s_ContactForm.find('input[name="contact[email]"]');
        var s_SubjectField = s_ContactForm.find('input[name="contact[subject]"]');
        var s_MessageField = s_ContactForm.find('textarea');

        /**
         * @param email
         */
        var ValidateEmail = function(email)
        {
            var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
            return re.test(email);
        };

        s_ContactForm.submit(function() {
            if(s_ContactForm.data('submitting'))
            {
                return false;
            }

            if($.trim(s_NameField.val()) == '')
            {
                alert("Please enter your first and last name, then try again.");
                s_NameField.focus();
                return false;
            }

            if($.trim(s_EmailField.val()) == '')
            {
                alert("Please enter your email address, then try again.");
                s_EmailField.focus();
                return false;
            }

            if(!ValidateEmail($.trim(s_EmailField.val())))
            {
                alert("The email address you have entered is invalid. Please enter a valid email address, then try again.");
                s_EmailField.focus();
                return false;
            }

            if($.trim(s_SubjectField.val()) == '')
            {
                alert("Please enter a subject line, then try again.");
                s_SubjectField.focus();
                return false;
            }

            if($.trim(s_MessageField.val()) == '')
            {
                alert("Please enter a message, then try again.");
                s_MessageField.focus();
                return false;
            }

            s_ContactForm.data('submitting', true);

            $.ajax({
                type: s_ContactForm.attr('method'),
                url: s_ContactForm.attr('action'),
                data: s_ContactForm.serialize(),
                complete: function(xhr) {
                    if(xhr.status >= 400)
                    {
                        s_ContactForm.data('submitting', false);
                        alert("An unexpected error has occured, please try again later.");
                        return;
                    }

                    s_ContactForm.data('submitting', false);
                    alert("Thank you for sending us an email! We will get back to you shortly via your email address.");
                    document.location.reload();
                }
            });

            return false;
        });
    });
})();