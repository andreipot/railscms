(function() {
    $(document).ready(function() {
        var s_ProjectPlannerForm = $('#new_project_planner');
        var s_FullNameField = $('#project_planner_full_name');
        var s_PhoneNumberField = $('#project_planner_phone_number');
        var s_EmailAddressField = $('#project_planner_email_address');

        /**
         * @param email
         */
        var ValidateEmail = function(email)
        {
            var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
            return re.test(email);
        };

        s_ProjectPlannerForm.submit(function() {
            if(s_ProjectPlannerForm.data('submitting'))
            {
                return false;
            }

            if($.trim(s_FullNameField.val()) == '')
            {
                alert("Please enter your first and last name, then try again.");
                s_FullNameField.focus();
                return false;
            }

            if($.trim(s_FullNameField.val()) == '')
            {
                alert("Please enter your first and last name, then try again.");
                s_FullNameField.focus();
                return false;
            }

            if($.trim(s_PhoneNumberField.val()) == '')
            {
                alert("Please enter your phone number, then try again.");
                s_PhoneNumberField.focus();
                return false;
            }

            if($.trim(s_EmailAddressField.val()) == '')
            {
                alert("Please enter your email address, then try again.");
                s_EmailAddressField.focus();
                return false;
            }

            if(!ValidateEmail($.trim(s_EmailAddressField.val())))
            {
                alert("The email address you have entered is invalid. Please enter a valid email address, then try again.");
                s_EmailAddressField.focus();
                return false;
            }

            s_ProjectPlannerForm.data('submitting', true);

            $.ajax({
                type: s_ProjectPlannerForm.attr('method'),
                url: s_ProjectPlannerForm.attr('action'),
                data: s_ProjectPlannerForm.serialize(),
                complete: function(xhr) {
                    if(xhr.status >= 400)
                    {
                        s_ProjectPlannerForm.data('submitting', false);
                        alert("An unexpected error has occured, please try again later.");
                        return;
                    }

                    s_ProjectPlannerForm.data('submitting', false);
                    alert("Thank you for using our project planner! We will get back to you shortly.");

                    document.location.reload();
                }
            });

            return false;
        });


    });
})();