(function() {
    $.fn.asdfSlider = function () {
        var elements = $(this);

        elements.each(function() {
            var element = $(this);

            var slider_identity = element.attr('data-slider-identity');
            var slide_container = element.find('.slide-container');
            var slides = slide_container.find('.slide');
            var slide_count = slides.length;
            var button_wrapper = $('.slider-buttons[data-slider-link="' + slider_identity + '"]');

            var current_slide = 0;

            if (slide_count < 2)
                return;

            var temp_slide_index = 0;

            slides.each(function() {
                var slide = $(this);

                slide.attr('data-slide-index', temp_slide_index++);
            });

            var UpdateSlideButtonClasses = function() {
                button_wrapper.find('.slider-button').removeClass('active');

                button_wrapper.find('.slider-button').eq(current_slide).addClass('active');
            };

            var GoToNextSlide = function () {
                var current_slide_element = slide_container.find('.slide').first();

                current_slide_element.remove();

                slide_container.append(current_slide_element);

                current_slide++;

                if(current_slide >= slide_count) {
                    current_slide = 0;
                }

                UpdateSlideButtonClasses();
            };

            var GoToPreviousSlide = function() {
                var last_slide_element = slide_container.find('.slide').last();

                last_slide_element.remove();

                slide_container.prepend(last_slide_element);

                current_slide--;

                if(current_slide < 0) {
                    current_slide = slide_count - 1;
                }

                UpdateSlideButtonClasses();
            };

            var GoToSlide = function(slide_index) {
                if(current_slide == slide_index)
                {
                    return;
                }

                if(current_slide > slide_index)
                {
                    while(current_slide != slide_index)
                    {
                        GoToPreviousSlide();
                    }

                    return;
                }

                while(current_slide != slide_index)
                {
                    GoToNextSlide();
                }
            };

            var button_index = 0;

            button_wrapper.find('.slider-button').each(function() {
                var slider_button = $(this);

                slider_button.attr('data-slide-index', button_index);

                slider_button.click(function() {
                    var slide_index = slider_button.attr('data-slide-index');

                    GoToSlide(slide_index);

                    return false;
                });

                button_index++;
            });

            var transition_interval = setInterval(function () {
                GoToNextSlide();
            }, 5000);

            var FixSlideSize = function() {
                var slider_width = element.width();

                slides.css('width', slider_width);
            };

            $(window).resize(function() {
                FixSlideSize();
            });

            FixSlideSize();
            UpdateSlideButtonClasses();
        });
    };

    $(document).ready(function() {
        $('#slider').asdfSlider();
    });
})();