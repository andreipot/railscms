ActiveAdmin.register Work do
  before_filter :only => [:show, :edit, :update, :destroy] do
    @work = Work.find_by(short_link:params[:id])
  end
  permit_params :short_link, :title, :tags, :order,  :list_page_description, :detail_page_link_text, :detail_page_link_url, :detail_page_description, :image

  filter :title
  filter :tags
  filter :order
  filter :short_link
  filter :detail_page_description
  filter :detail_page_link_url
  #filter :work_slider_id

  #index form

  index  do
    column :short_link
    column :title
    column :tags
    column :order
   # column :work_slider_id
=begin
    column :"Work Slider" do |work|
      content_tag(:p, work.work_slider.work_short_link)
    end
=end
    column :list_page_description
    column :detail_page_link_text
    column :detail_page_link_url
    column :detail_page_description
    column "Image" do |work|
      image_tag(work.image.url(:thumb), :height => '100')
    end
    actions
  end


  #edit form

  form do |f|
    f.inputs  "Work Details" do
      f.input :short_link
      f.input :title
      f.input :tags #, as: :select, collection: Service.all.collect { |service| [service.short_link, service.short_link]},  :multiple => true
      f.input :order
      f.input :list_page_description
      f.input :detail_page_link_text
      f.input :detail_page_link_url
      f.input :detail_page_description
      f.input :image, :as => :file, :required => false, :hint => f.template.image_tag(f.object.image.url(:medium))
    end
    f.actions
  end

  #show form
  show do |ad|
    attributes_table do
      row :short_link
      row :title
      row :tags
      row :order
     # row :work_slider_id
      row :list_page_description
      row :detail_page_link_text
      row :detail_page_link_url
      row :detail_page_description
      row :image do
        image_tag(ad.image.url(:thumb))
      end
      # Will display the image on show object page
    end
  end
end
