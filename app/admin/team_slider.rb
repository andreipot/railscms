ActiveAdmin.register TeamSlider do

  permit_params :team_id,  :order, :image, :title, :work_short_link, :website_text, :website_link
  filter :team_id
  filter :title
  filter :order
  filter :work_short_link
  filter :website_text
  filter :website_link

  #index form

  index  do
    column "Team" do |team_slider|
      unless team_slider.team.nil?
       team_slider.team.short_link
      end
    end
    column :order
    column :title
    column :work_short_link
    column :website_text
    column :website_link
    column "Image" do |team_slider|
      image_tag(team_slider.image.url(:thumb), :height => '100')
    end
    actions
  end


  #edit form

  form do |f|
    f.inputs  "Team Slider Details" do
      f.input :team_id, as: :select, collection: Team.all.collect {|team| [team.short_link, team.id]}
      f.input :order
      f.input :title
      f.input :work_short_link
      f.input :website_text
      f.input :website_link
      f.input :image, :as => :file, :required => false, :hint => f.template.image_tag(f.object.image.url(:medium))
      end
      f.actions
  end

  #show form
  show do |ad|
    attributes_table do
      row :team_id
      row :order
      row :title
      row :work_short_link
          row :website_text
          row :website_link
          row :image do
            image_tag(ad.image.url(:thumb))
          end
      # Will display the image on show object page
    end
  end
end
