ActiveAdmin.register SiteConfig do
  permit_params :key, :value

  filter :key

  #index form

  index  do
    column :key
    column :value
    actions
  end


  #edit form

  form do |f|
    f.inputs  "Config Details" do
      f.input :key
      f.input :value
    end
    f.actions
  end

  #show form
  show do |ad|
    attributes_table do
      row :key
      row :value
    end
  end
end
