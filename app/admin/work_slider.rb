ActiveAdmin.register WorkSlider do

  permit_params :work_id,:work_short_link, :order, :navigation_image, :navigation_url, :active_class, :active_html

  filter :active_class
  filter :order
  filter :navigation_url
  filter :work_short_link
  filter :active_html

  #index form
  index  do
   column "Work" do |work_slider|
     unless work_slider.work.nil?
      work_slider.work.title
    end
   end
   column :order
   column :navigation_url
   column :active_class
   column :active_html
   column "Navigation Image" do |work_slider|
     image_tag(work_slider.navigation_image.url(:thumb), :height => '100')
   end
    actions
  end

  #edit form

  form do |f|
    f.inputs  "Work Slider Details" do
      f.input :work_id, as: :select, collection: Work.all.collect {|work| [work.title, work.id]}
      f.input :order
      f.input :navigation_url
      f.input :active_class
      f.input :active_html
      f.input :navigation_image, :as => :file, :required => false, :hint => f.template.image_tag(f.object.navigation_image.url(:medium))
    end
    f.actions
  end

  #show form
  show do |ad|
    attributes_table do
      row :work_id
      row :order
      row :navigation_url
      row :active_class
      row :active_html
      row :navigation_image do
        image_tag(ad.navigation_image.url(:thumb))
      end
      # Will display the image on show object page
    end
  end
end
