ActiveAdmin.register Contact do
  permit_params :email, :full_name, :company, :subject, :message

  filter :email
  filter :full_name
  filter :company
  filter :subject

  #index form

  index  do
    column :email
    column :full_name
    column :subject
    column :company
    column :message

    actions
  end

  #edit form

  form do |f|
    f.inputs  "Contact Details" do
      f.input :email
      f.input :full_name
      f.input :subject
      f.input :company
      f.input :message
    end
    f.actions
  end

end
