ActiveAdmin.register Service do
  before_filter :only => [:show, :edit, :update, :destroy] do
    @service = Service.find_by(short_link:params[:id])
  end
  permit_params :short_link, :name, :order, :homepage_icon, :homepage_icon_hover, :homepage_description, :detail_top_description, :detail_icon_css_class, :detail_first_heading, :detail_first_paragraph, :detail_second_heading, :detail_second_paragraph, :detail_third_heading, :detail_third_paragrah

  filter :name
  filter :short_link
  filter :detail_first_heading
  filter :detail_second_heading
  filter :detail_third_heading
  filter :detail_icon_css_class
  filter :homepage_icon

  #index form

  index  do
    column :short_link
    column :name
    column :order
=begin
    column :service_slider_id do |service|
      content_tag(:p, service.service_slider.inspect)
    end
=end
    column :homepage_description
    column :detail_top_description
    column :detail_icon_css_class
    column :detail_first_heading
    column :detail_first_paragraph
    column :detail_second_heading
    column :detail_second_paragraph
    column :detail_third_heading
    column :detail_third_paragrah
    column "Homepage Icon" do |service|
      image_tag(service.homepage_icon.url(:thumb), :height => '100')
    end
    column "Homepage Icon Hover" do |service|
      image_tag(service.homepage_icon_hover.url(:thumb), :height => '100')
    end
    actions
  end

  #edit form

  form do |f|
    f.inputs  "Service Details" do
      f.input :short_link
      f.input :name
      f.input :order
      f.input :homepage_description
      f.input :detail_top_description
      f.input :detail_icon_css_class
      f.input :detail_first_heading
      f.input :detail_first_paragraph
      f.input :detail_second_heading
      f.input :detail_second_paragraph
      f.input :detail_third_heading
      f.input :detail_third_paragrah
      f.input :homepage_icon, :as => :file, :required => false, :hint => f.template.image_tag(f.object.homepage_icon.url(:medium))
      f.input :homepage_icon_hover, :as => :file, :required => false, :hint => f.template.image_tag(f.object.homepage_icon_hover.url(:medium))
    end
    f.actions
  end

  #show form
  show do |ad|
    attributes_table do
      row :short_link
      row :name
      row :order
      row :homepage_description
      row :detail_top_description
      row :detail_icon_css_class
      row :detail_first_heading
      row :detail_first_paragraph
      row :detail_second_heading
      row :detail_second_paragraph
      row :detail_third_heading
      row :detail_third_paragrah
      row :homepace_icon do
        image_tag(ad.homepage_icon.url(:thumb))
      end
      row :homepace_icon_hover do
        image_tag(ad.homepage_icon_hover.url(:thumb))
      end
      # Will display the image on show object page
    end
  end
end
