ActiveAdmin.register Team do
  before_filter :only => [:show, :edit, :update, :destroy] do
    @team = Team.find_by(short_link:params[:id])
  end
  permit_params :short_link, :position, :order,  :image, :team_page_bio, :detail_page_website_text, :detail_page_website_url, :detail_page_bio, :facebook_url, :twitter_url, :gplus_url, :dribble_url, :github_url

  filter :short_link
  filter :position
  filter :order
  filter :team_slider_id
  filter :team_page_bio
  filter :detail_page_bio
  filter :detail_page_website_url
  filter :facebook_url
  filter :twitter_url
  filter :gplus_url
  filter :dribble_url
  filter :github_url

  #index form

  index  do
    column :short_link
    column :position
    column :order
=begin
    column :team_slider_id do |team|
      content_tag(:p, team.team_slider.title)
    end
=end
    column "Image" do |team|
      image_tag(team.image.url(:thumb), :height => '100')
    end
    column :team_page_bio
    column :detail_page_website_text
    column :detail_page_website_url
    column :detail_page_bio
    column :facebook_url
    column :twitter_url
    column :gplus_url
    column :dribble_url
    column :github_url

    actions
  end


  #edit form

  form do |f|
    f.inputs  "Team Details" do
      f.input :short_link
      f.input :position
      f.input :order
      f.input :image, :as => :file, :required => false, :hint => f.template.image_tag(f.object.image.url(:medium))
      f.input :team_page_bio
      f.input :detail_page_website_text
      f.input :detail_page_website_url
      f.input :detail_page_bio
      f.input :facebook_url
      f.input :twitter_url
      f.input :gplus_url
      f.input :dribble_url
      f.input :github_url
    end
    f.actions
  end

  #show form
  show do |ad|
    attributes_table do
      row :short_link
      row :position
      row :order
      row :team_page_bio
      row :detail_page_website_text
      row :detail_page_website_url
      row :detail_page_bio
      row :facebook_url
      row :twitter_url
      row :gplus_url
      row :dribble_url
      row :github_url
      row :image do
        image_tag(ad.image.url(:thumb))
      end
      # Will display the image on show object page
    end
  end

end
