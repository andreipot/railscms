ActiveAdmin.register ProjectPlanner do

  permit_params :full_name, :company, :phone_number, :email_address, :hear_about_us, :project_url, :launch_date, :budget, :site_concept, :main_objective, :schedule_considerations

  filter :full_name
  filter :company
  filter :phone_number
  filter :email_address
  filter :budget, as: :select, collection: [['Under $5,000', '5000'], ['$5,000 - $10,000','10000'], ['$10,000 - $30,000','30000'], ['$30,000 - $50,000+','50000'], ['Unknown','Unknown']], value: :budget
  filter :site_concept
  filter :main_objective

  #index form

  index  do
    column :full_name
    column :company
    column :phone_number
    column :email_address
    column :hear_about_us
    column :project_url
    column :launch_date
    column :budget
    column :site_concept
    column :main_objective
    column :schedule_considerations
    actions
  end

  #edit form

  form do |f|
    f.inputs  "Project Plan Details" do
      f.input :full_name
      f.input :company
      f.input :phone_number
      f.input :email_address
      f.input :hear_about_us
      f.input :project_url
      f.input :launch_date
      f.input :budget, as: :select, collection: [['Under $5,000', '5000'], ['$5,000 - $10,000','10000'], ['$10,000 - $30,000','30000'], ['$30,000 - $50,000+','50000'], ['Unknown','Unknown']], value: :budget, :include_blank => false
      f.input :site_concept
      f.input :main_objective
      f.input :schedule_considerations
    end
    f.actions
  end

end
