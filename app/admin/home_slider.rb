ActiveAdmin.register HomeSlider do

  permit_params :layout, :order, :title, :description, :link, :tags, :image

  filter :layout ,:as => :select ,collection: [['Intro Layout', 'slide-intro-layout'], ['Column Layout','slide-column-layout']]
  filter :title
  filter :tags
  filter :link

  #index form

  index  do
    column :title
    column :layout
    column :tags
    column :link
    column :order
    column "Image" do |home_slider|
      image_tag(home_slider.image.url(:thumb), :height => '100')
    end
    actions
  end

  #edit form

  form do |f|
    f.inputs  "Homeslider Details" do
    f.input :title
    f.input :layout, as: :select, collection: [['Intro Layout', 'slide-intro-layout'], ['Column Layout','slide-column-layout']], value: :layout ,:include_blank => false
    f.input :tags
    f.input :description
    f.input :link
    f.input :order
    f.input :image, :as => :file, :required => false, :hint => f.template.image_tag(f.object.image.url(:medium))
  end
    f.actions
  end


  #show form
  show do |ad|
    attributes_table do
      row :title
      row :layout
      row :tags
      row :description
      row :link
      row :order
      row :image do
        image_tag(ad.image.url(:thumb))
      end
      # Will display the image on show object page
    end
  end
end
