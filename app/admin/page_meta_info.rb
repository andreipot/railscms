ActiveAdmin.register PageMetaInfo do
  permit_params :uri, :keywords, :description, :title

  filter :keywords
  filter :uri
  filter :description
  filter :title

  #index form

  index  do
    column :uri
    column :keywords
    column :description
    column :title
    actions
  end

  #edit form

  form do |f|
    f.inputs  "Peta Meta Details" do
      f.input :uri
      f.input :keywords
      f.input :description
      f.input :title  
    end
    f.actions
  end

end
