ActiveAdmin.register ServiceSlider do
  permit_params :service_id, :order, :active_class, :active_html, :navigation_image

  #filter :service_short_description
  filter :active_class
  filter :order

  #index form
  index  do
    column "Service" do |service_slider|
      unless service_slider.service.nil?
        service_slider.service.name
      end
    end
    #column :service_short_description
    column :active_class
    column :active_html
    column :order
    column "Navigation Image" do |service_slider|
      image_tag(service_slider.navigation_image.url(:thumb), :height => '100')
    end
    actions
  end


  #edit form

  form do |f|
    f.inputs  "Service Details" do
      f.input :service_id, as: :select, collection: Service.all.collect {|service| [service.short_link, service.id]}
      #f.input :service_short_description
      f.input :active_class
      f.input :active_html
      f.input :order
      f.input :navigation_image, :as => :file, :required => false, :hint => f.template.image_tag(f.object.navigation_image.url(:medium))
    end
    f.actions
  end

  #show form
  show do |ad|
    attributes_table do
      row :service_id
      #row :service_short_description
      row :active_class
      row :active_html
      row :order
      row :navigation_image do
        image_tag(ad.navigation_image.url(:thumb))
      end
      # Will display the image on show object page
    end
  end
end
