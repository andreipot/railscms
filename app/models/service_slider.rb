class ServiceSlider < ActiveRecord::Base
  has_attached_file :navigation_image, :styles => { :medium => "300x300>", :thumb => "100x100>" }, :default_url => "/images/:style/missing.png"
  validates_attachment :navigation_image, content_type: { content_type: ["image/jpg", "image/jpeg",   "image/png"] }

  belongs_to :service
end
