class Service < ActiveRecord::Base
  has_attached_file :homepage_icon, :styles => { :medium => "300x300>", :thumb => "100x100>" }
  has_attached_file :homepage_icon_hover, :styles => { :medium => "300x300>", :thumb=> "100x100>" }

  validates_attachment :homepage_icon, content_type: { content_type: ["image/jpg", "image/jpeg",   "image/png"] }
  validates_attachment :homepage_icon_hover, content_type: { content_type: ["image/jpg", "image/jpeg",   "image/png"] }

  has_many :service_sliders

  def to_param
    short_link
  end
end
