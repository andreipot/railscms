class WorkSlidersController < InheritedResources::Base

  private

    def work_slider_params
      params.require(:work_slider).permit(:work_short_description, :order, :navigation_image, :navigation_url, :active_class, :active_html)
    end
end

