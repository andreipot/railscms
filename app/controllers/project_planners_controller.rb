class ProjectPlannersController < ApplicationController
  before_action :set_project_planner, only: [:show, :edit, :update, :destroy]

  # GET /project_planners
  # GET /project_planners.json
  def index
    @project_planners = ProjectPlanner.all
  end

  # GET /project_planners/1
  # GET /project_planners/1.json
  def show
  end

  # GET /project_planners/new
  def new
    @project_planner = ProjectPlanner.new
  end

  # GET /project_planners/1/edit
  def edit
  end

  # POST /project_planners
  # POST /project_planners.json
  def create
    @project_planner = ProjectPlanner.new(project_planner_params)

    respond_to do |format|
      if @project_planner.save
        #format.html { redirect_to @project_planner, notice: 'Project planner was successfully created.' }
        format.html { render :new }
        format.json { render :show, status: :created, location: @project_planner }
      else
        format.html { render :new }
        format.json { render json: @project_planner.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /project_planners/1
  # PATCH/PUT /project_planners/1.json
  def update
    respond_to do |format|
      if @project_planner.update(project_planner_params)
        format.html { redirect_to @project_planner, notice: 'Project planner was successfully updated.' }
        format.json { render :show, status: :ok, location: @project_planner }
      else
        format.html { render :edit }
        format.json { render json: @project_planner.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /project_planners/1
  # DELETE /project_planners/1.json
  def destroy
    @project_planner.destroy
    respond_to do |format|
      format.html { redirect_to project_planners_url, notice: 'Project planner was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_project_planner
      @project_planner = ProjectPlanner.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def project_planner_params
      params.require(:project_planner).permit(:full_name, :company, :phone_number, :email_address, :hear_about_us, :project_url, :launch_date, :budget, :site_concept, :main_objective, :schedule_considerations)
    end
end
