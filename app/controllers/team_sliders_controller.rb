class TeamSlidersController < InheritedResources::Base

  private

    def team_slider_params
      params.require(:team_slider).permit(:team_member_short_link, :order, :image, :title, :work_short_link, :website_text, :website_link)
    end
end

