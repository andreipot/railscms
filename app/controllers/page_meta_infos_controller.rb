class PageMetaInfosController < InheritedResources::Base

  private

    def page_meta_info_params
      params.require(:page_meta_info).permit(:uri, :keywords, :description, :title)
    end
end

