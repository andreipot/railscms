class ServiceSlidersController < InheritedResources::Base

  private

    def service_slider_params
      params.require(:service_slider).permit(:service_short_description, :order, :active_class, :active_html, :navigation_image)
    end
end

