class HomeSlidersController < InheritedResources::Base


  private

    def home_slider_params
      params.require(:home_slider).permit(:layout, :title, :description, :link, :tags, :image)
    end
end

