class SiteConfigsController < InheritedResources::Base

  private

    def site_config_params
      params.require(:site_config).permit(:key, :value)
    end
end

