class PageSettingsController < ApplicationController
  before_action :set_page_setting, only: [:show, :edit, :update, :destroy]

  # GET /page_settings
  # GET /page_settings.json
  def index
    @page_settings = PageSetting.all
  end

  # GET /page_settings/1
  # GET /page_settings/1.json
  def show
  end

  # GET /page_settings/new
  def new
    @page_setting = PageSetting.new
  end

  # GET /page_settings/1/edit
  def edit
  end

  # POST /page_settings
  # POST /page_settings.json
  def create
    @page_setting = PageSetting.new(page_setting_params)

    respond_to do |format|
      if @page_setting.save
        format.html { redirect_to @page_setting, notice: 'Page setting was successfully created.' }
        format.json { render :show, status: :created, location: @page_setting }
      else
        format.html { render :new }
        format.json { render json: @page_setting.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /page_settings/1
  # PATCH/PUT /page_settings/1.json
  def update
    respond_to do |format|
      if @page_setting.update(page_setting_params)
        format.html { redirect_to @page_setting, notice: 'Page setting was successfully updated.' }
        format.json { render :show, status: :ok, location: @page_setting }
      else
        format.html { render :edit }
        format.json { render json: @page_setting.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /page_settings/1
  # DELETE /page_settings/1.json
  def destroy
    @page_setting.destroy
    respond_to do |format|
      format.html { redirect_to page_settings_url, notice: 'Page setting was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_page_setting
      @page_setting = PageSetting.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def page_setting_params
      params[:page_setting]
    end
end
