class HomeController < ApplicationController
  def index
    @services = Service.all
    @home_sliders = HomeSlider.order('home_sliders.order').all
  end
end
