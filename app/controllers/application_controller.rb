class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception
  before_action :set_footer_urls
  def set_footer_urls
    @SiteConfig = SiteConfig.all
    @facebook_url = SiteConfig.where(key: 'facebook_url').first
    @twitter_url  = SiteConfig.where(key: 'facebook_url').first
    @gplus_url    = SiteConfig.where(key: 'facebook_url').first
    @github_url   = SiteConfig.where(key: 'facebook_url').first
  end
end
