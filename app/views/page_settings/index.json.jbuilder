json.array!(@page_settings) do |page_setting|
  json.extract! page_setting, :id
  json.url page_setting_url(page_setting, format: :json)
end
