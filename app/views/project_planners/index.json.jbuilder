json.array!(@project_planners) do |project_planner|
  json.extract! project_planner, :id, :full_name, :company, :phone_number, :email_address, :hear_about_us, :project_url, :launch_date, :budget, :site_concept, :main_objective, :schedule_considerations
  json.url project_planner_url(project_planner, format: :json)
end
