json.array!(@home_sliders) do |home_slider|
  json.extract! home_slider, :id, :layout, :title, :description, :link, :tags, :image
  json.url home_slider_url(home_slider, format: :json)
end
