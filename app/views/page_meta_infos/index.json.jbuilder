json.array!(@page_meta_infos) do |page_meta_info|
  json.extract! page_meta_info, :id, :uri, :keywords, :description, :title
  json.url page_meta_info_url(page_meta_info, format: :json)
end
