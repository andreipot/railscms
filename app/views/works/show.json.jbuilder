json.extract! @work, :id, :short_link, :title, :tags, :order, :list_page_description, :detail_page_link_text, :detail_page_link_url, :detail_page_description, :image, :created_at, :updated_at
