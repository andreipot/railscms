json.array!(@team_sliders) do |team_slider|
  json.extract! team_slider, :id, :team_member_short_link, :order, :image, :title, :work_short_link, :website_text, :website_link
  json.url team_slider_url(team_slider, format: :json)
end
