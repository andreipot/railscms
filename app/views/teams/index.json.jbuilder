json.array!(@teams) do |team|
  json.extract! team, :id, :short_link, :position, :order, :image, :team_page_bio, :detail_page_website_text, :detail_page_website_url, :detail_page_bio, :facebook_url, :twitter_url, :gplus_url, :dribble_url, :github_url
  json.url team_url(team, format: :json)
end
