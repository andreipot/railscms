json.array!(@work_sliders) do |work_slider|
  json.extract! work_slider, :id, :work_short_description, :order, :navigation_image, :navigation_url, :active_class, :active_html
  json.url work_slider_url(work_slider, format: :json)
end
