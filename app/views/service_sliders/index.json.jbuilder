json.array!(@service_sliders) do |service_slider|
  json.extract! service_slider, :id, :service_short_description, :order, :active_class, :active_html, :navigation_image
  json.url service_slider_url(service_slider, format: :json)
end
